"""
# ------------------- Application to Real Data ---------------------- #
For a given signal or given signals, one can recreate a probabilistic
graphical model (PGM) with the CL algorithm. Applied on an acceleration
signal one can choose to use some filter of the kinematic frequencies
if given. Default is the data of matlab. The code provided is directly
applicable to the MATLAB data set of a bearing fault, see
https://de.mathworks.com/help/predmaint/ug/Rolling-Element-Bearing-Fault-Diagnosis.html.
Otherwise, the user has to adapt the script for own data. For this adapt the
function load_data() in ApplicationRealData_intern_function.py. Further make sure
all path are given correctly.
# ------------------------------------------------------------------- #

To run this script make sure the following packages are installed:
-numpy
-networkx
-scipy
-pgmpy
-matplotlib
-pandas

"""

from ApplicationRealData_intern_function import *

import warnings
warnings.filterwarnings('ignore')

if __name__ == '__main__':
    # Uncomment to generate the icons
    # generate_icons(freqencies)

    code = 'matlab'                     # choose data set
    weights_bool = True                 # display edge weights of the tree in plot if True
    plot_all = False                    # plot also signal data
    bandpass = False                    # compute bandpass filtered signal and reconstruct graph from this
    envelope_comp = False               # compute envelope signal and reconstruct graph from this, if bandpass True, this is also done for the filtered signal
    use_baseline_case = False           # use a reference signal (in given dataset signal 'Baseline 1', for own data set has to be defined
    draw_grey_edges = True              # draw complete graph but with non-edges in grey

    windowlength = int(np.ceil(96))     # choose windowlength for STFT
    overlap_percent = 0.75              # overlap for STFT
    percent_filter = 0.05               # bandpass filter, bandwidth in percent of the corresponding value of the frequency we want to filter
    cut_dataset = True                  # if True use a predefined length for the dataset for reconstruction
    len_dataset = 2000                  # length of the dataset for reconstruction
    num_nodes = 8                       # number of frequency bands per signal
    frequency_kurt = 2670.3             # frequency for the bandpass filter given by kurtosis
    bandwidth_kurt = 763                # bandwidth for bandpass filter given by kurtosis
    windowlength_kurt = 128             # windowlength kurtosis

    if code == 'matlab':
        ID_list = ['Baseline 1', 'Baseline 2', 'Baseline 3','Outer 1','Outer 2','Outer 3', 'Outer 4','Outer 5','Outer 6','Outer 7', 'Outer 8','Outer 9','Outer 10', 'Inner 1', 'Inner 2','Inner 3','Inner 4','Inner 5','Inner 6','Inner 7']
    elif code == 'your_own_signal':
        ID_list = []

    bounds = []
    bounds_env = []
    bounds_bp_kin = []
    bounds_env_bp_kin = []
    bounds_bp_kurt = []
    bounds_env_bp_kurt = []
    for ID in ID_list:
        print(ID)
        data = []
        data_env = []
        data_bp_kin = []
        data_bp_kurt = []
        data_env_bp_kin = []
        data_env_bp_kurt = []
        row_names = []
        filtered_signal = []
        filtered_signalAX = []
        filtered_signalBX = []
        path_icon = 'icons/sine_'
        icons = dict()
        targetsize = (100, 100)

        if code == 'matlab':
            path = 'MFPT Fault Data Sets/'
            signal, time, kinematicfq, fs, load = load_dataML(path, ID)
            freq_powerspectrum, spec_powerspectrum = welch(signal, fs, nperseg=windowlength, scaling='spectrum', noverlap=int(np.ceil(windowlength*overlap_percent)), window='hann')
            freq, time_stft, stft, smag, _ = gen_mag_fb(signal, fs, windowlength, overlap_percent)
            if envelope_comp == True:
                envelope = comp_envelope(signal, time, fs)
                freq_powerspectrum_env, spec_powerspectrum_env = welch(envelope, fs, nperseg=windowlength, scaling='density', noverlap=int(np.ceil(windowlength*overlap_percent)))
                freq_env, time_stft_env, stft_env, smag_env, _ = gen_mag_fb(envelope, fs, windowlength, overlap_percent)
            if bandpass == True:
                signal_filtered_kurtosis = bandpass_filter(signal, frequency_kurt - bandwidth_kurt / 2, frequency_kurt + bandwidth_kurt / 2, fs, windowlength_kurt)
                freq_bp_kurt, time_stft_bp_kurt, stft_bp_kurt, smag_bp_kurt, _ = gen_mag_fb(signal_filtered_kurtosis, fs, windowlength, overlap_percent)
                for kin in kinematicfq:
                    signal_filt_kin = bandpass_filter(signal, (1 - percent_filter) * kin, (1 + percent_filter) * kin, fs, 2)
                    if len(filtered_signal) == 0:
                        filtered_signal = np.reshape(signal_filt_kin, (len(signal_filt_kin), 1))
                    else:
                        filtered_signal = np.concatenate((filtered_signal, np.reshape(signal_filt_kin, (len(signal_filt_kin), 1))), axis=1)
                kinematic_filtered_signal = np.sum(filtered_signal, axis=1)
                freq_bp_kin, time_stft_bp_kin, stft_bp_kin, smag_bp_kin, _ = gen_mag_fb(kinematic_filtered_signal, fs, windowlength, overlap_percent)
                if envelope_comp == True:
                    envelope_bp_kurt = comp_envelope(signal_filtered_kurtosis, time, fs)
                    freq_powerspectrum_env_bp_kurt, spec_powerspectrum_env_bp_kurt = welch(envelope_bp_kurt, fs, nperseg=2048 * 32, scaling='density', noverlap=int(np.ceil(2048*16 * 0.90)))
                    freq_env_bp_kurt, time_stft_env_bp_kurt, stft_env_bp_kurt, smag_env_bp_kurt, _ = gen_mag_fb(envelope_bp_kurt, fs, windowlength, overlap_percent)
                    envelope_bp_kin = comp_envelope(kinematic_filtered_signal, time, fs)
                    freq_powerspectrum_env_bp_kin, spec_powerspectrum_env_bp_kin = welch(envelope_bp_kin, fs, nperseg=2048 * 16, scaling='density', noverlap=int(np.ceil(2048 *8* 0.90)))
                    freq_env_bp_kin, time_stft_env_bp_kin, stft_env_bp_kin, smag_env_bp_kin, _ = gen_mag_fb(envelope_bp_kin, fs, windowlength, overlap_percent)
            if plot_all == True:
                plot_signal(signal[0:3000], time[0:3000], xlab='Time (seconds)', title='Acceleration ' + ID, save=True)
                plot_signal_kin(kinematicfq,20*np.log10(spec_powerspectrum)[0:90], freq_powerspectrum[0:90], xlab='Frequency (Hz)', ylab='Power (dB)', title='Power Signal ' + ID, save=True)
                plot_signal(smag, time=time_stft, freq=freq, xlab='Time', ylab='Frequency', colorbarlab='dB', title='Spectral Power', colmesh=True)
                if envelope_comp == True:
                    plot_signal(envelope[0:3000], time[0:3000], xlab='Time (seconds)', title='Envelope ' + ID, save=True)
                    plot_signal_kin(kinematicfq,spec_powerspectrum_env[0:3000], freq_powerspectrum_env[0:3000], xlab='Frequency (Hz)', ylab='Peak Amplitude', title='Power Envelope ' + ID, save=True)
                if bandpass == True:
                    plot_signal(signal_filtered_kurtosis[0:3000], time[0:3000], xlab='Time (seconds)', title='Acceleration Bandpass ' + ID, save=True)
                    plot_signal(kinematic_filtered_signal[0:3000], time[0:3000], xlab='Time (seconds)', title='Acceleration Bandpass Kinematic' + ID, save=True)
                    if envelope_comp == True:
                        plot_signal(envelope_bp_kurt[0:3000], time[0:3000], xlab='Time (seconds)', title='Envelope Bandpass ' + ID, save=True)
                        plot_signal_kin(kinematicfq, freq_powerspectrum_env_bp_kurt[0:1400], spec_powerspectrum_env_bp_kurt[0:1400], xlab='Frequency (Hz)', ylab='Peak Amplitude', title='Power Envelope Bandpass ' + ID, save=True)

            freq_range = np.ceil(freq[0:num_nodes + 1])
            for i in np.arange(len(freq_range) - 1):
                print('Frequency Band: ', freq_range[i], freq_range[i + 1])
                data_FB = smag[i, :]
                if envelope_comp == True:
                    data_FB_env = smag_env[i, :]
                if bandpass == True:
                    data_FB_bp_kurt = smag_bp_kurt[i, :]
                    data_FB_bp_kin = smag_bp_kin[i, :]
                    if envelope_comp == True:
                        data_FB_env_bp_kurt = smag_env_bp_kurt[i, :]
                        data_FB_env_bp_kin = smag_env_bp_kin[i, :]
                if use_baseline_case == True:
                    if ID == 'Baseline 1':
                        data_states, bounds_states = comp_statevector(data_FB)
                        if len(bounds) == 0:
                            bounds = np.reshape(bounds_states, (len(bounds_states), 1))
                        else:
                            bounds = np.concatenate((bounds, np.reshape(bounds_states, (len(bounds_states), 1))), axis=1)
                        if len(data) == 0:
                            data = np.reshape(data_states, (len(data_states), 1))
                        else:
                            data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                        if envelope_comp == True:
                            data_states_env, bounds_states_env = comp_statevector(data_FB_env)
                            if len(bounds_env) == 0:
                                bounds_env = np.reshape(bounds_states_env, (len(bounds_states_env), 1))
                            else:
                                bounds_env = np.concatenate((bounds_env, np.reshape(bounds_states_env, (len(bounds_states_env), 1))), axis=1)
                            if len(data_env) == 0:
                                data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                            else:
                                data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                        if bandpass == True:
                            data_states_bp_kin, bounds_states_bp_kin = comp_statevector(data_FB_bp_kin)
                            data_states_bp_kurt, bounds_states_bp_kurt = comp_statevector(data_FB_bp_kurt)
                            if len(bounds_bp_kin) == 0:
                                bounds_bp_kin = np.reshape(bounds_states_bp_kin, (len(bounds_states_bp_kin), 1))
                            else:
                                bounds_bp_kin = np.concatenate((bounds_bp_kin, np.reshape(bounds_states_bp_kin, (len(bounds_states_bp_kin), 1))), axis=1)
                            if len(data_bp_kin) == 0:
                                data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                            else:
                                data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                            if len(bounds_bp_kurt) == 0:
                                bounds_bp_kurt = np.reshape(bounds_states_bp_kurt, (len(bounds_states_bp_kurt), 1))
                            else:
                                bounds_bp_kurt = np.concatenate((bounds_bp_kurt, np.reshape(bounds_states_bp_kurt, (len(bounds_states_bp_kurt), 1))), axis=1)
                            if len(data_bp_kurt) == 0:
                                data_bp_kurt = np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))
                            else:
                                data_bp_kurt = np.concatenate((data_bp_kurt, np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))), axis=1)
                            if envelope_comp == True:
                                data_states_env_bp_kin, bounds_states_env_bp_kin = comp_statevector(data_FB_env_bp_kin)
                                data_states_env_bp_kurt, bounds_states_env_bp_kurt = comp_statevector(data_FB_env_bp_kurt)
                                if len(bounds_env_bp_kin) == 0:
                                    bounds_env_bp_kin = np.reshape(bounds_states_env_bp_kin, (len(bounds_states_env_bp_kin), 1))
                                else:
                                    bounds_env_bp_kin = np.concatenate((bounds_env_bp_kin, np.reshape(bounds_states_env_bp_kin, (len(bounds_states_env_bp_kin), 1))), axis=1)
                                if len(data_env_bp_kin) == 0:
                                    data_env_bp_kin = np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))
                                else:
                                    data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                                if len(bounds_env_bp_kurt) == 0:
                                    bounds_env_bp_kurt = np.reshape(bounds_states_env_bp_kurt, (len(bounds_states_env_bp_kurt), 1))
                                else:
                                    bounds_env_bp_kurt = np.concatenate((bounds_env_bp_kurt, np.reshape(bounds_states_env_bp_kurt, (len(bounds_states_env_bp_kurt), 1))), axis=1)
                                if len(data_env_bp_kurt) == 0:
                                    data_env_bp_kurt = np.reshape(data_states_env_bp_kurt, (len(data_states_env_bp_kurt), 1))
                                else:
                                    data_env_bp_kurt = np.concatenate((data_env_bp_kurt, np.reshape(data_states_env_bp_kurt, (len(data_states_env_bp_kurt), 1))), axis=1)
                    else:
                        bounds_states = bounds[:, i]
                        data_states = np.digitize(data_FB, bounds_states)
                        if len(data) == 0:
                            data = np.reshape(data_states, (len(data_states), 1))
                        else:
                            data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                        if envelope_comp == True:
                            bounds_states_env = bounds_env[:, i]
                            data_states_env = np.digitize(data_FB_env, bounds_states_env)
                            if len(data_env) == 0:
                                data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                            else:
                                data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                        if bandpass == True:
                            bounds_states_bp_kin = bounds_bp_kin[:, i]
                            data_states_bp_kin = np.digitize(data_FB_bp_kin, bounds_states_bp_kin)
                            bounds_states_bp_kurt = bounds_bp_kurt[:, i]
                            data_states_bp_kurt = np.digitize(data_FB_bp_kurt, bounds_states_bp_kurt)
                            if len(data_bp_kin) == 0:
                                data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                            else:
                                data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                            if len(data_bp_kurt) == 0:
                                data_bp_kurt = np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))
                            else:
                                data_bp_kurt = np.concatenate((data_bp_kurt, np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))), axis=1)
                            if envelope_comp == True:
                                bounds_states_env_bp_kin = bounds_env_bp_kin[:, i]
                                data_states_env_bp_kin = np.digitize(data_FB_env_bp_kin, bounds_states_env_bp_kin)
                                bounds_states_env_bp_kurt = bounds_env_bp_kurt[:, i]
                                data_states_env_bp_kurt = np.digitize(data_FB_env_bp_kurt, bounds_states_env_bp_kurt)
                                if len(data_env_bp_kin) == 0:
                                    data_env_bp_kin = np.reshape(data_states_env_bp_kin,(len(data_states_env_bp_kin), 1))
                                else:
                                    data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                                if len(data_env_bp_kurt) == 0:
                                    data_env_bp_kurt = np.reshape(data_states_env_bp_kurt, (len(data_states_env_bp_kurt), 1))
                                else:
                                    data_env_bp_kurt = np.concatenate((data_env_bp_kurt, np.reshape(data_states_env_bp_kurt, (len(data_states_env_bp_kurt), 1))), axis=1)
                else:
                    data_states, bounds_states = comp_statevector(data_FB)
                    if len(data) == 0:
                        data = np.reshape(data_states, (len(data_states), 1))
                    else:
                        data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                    if envelope_comp == True:
                        data_states_env, bounds_states_env = comp_statevector(data_FB_env)
                        if len(data_env) == 0:
                            data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                        else:
                            data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                    if bandpass == True:
                        data_states_bp_kin, bounds_states_bp_kin = comp_statevector(data_FB_bp_kin)
                        data_states_bp_kurt, bounds_states_bp_kurt = comp_statevector(data_FB_bp_kurt)
                        if len(data_bp_kin) == 0:
                            data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                        else:
                            data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                        if len(data_bp_kurt) == 0:
                            data_bp_kurt = np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))
                        else:
                            data_bp_kurt = np.concatenate((data_bp_kurt, np.reshape(data_states_bp_kurt, (len(data_states_bp_kurt), 1))), axis=1)
                        if envelope_comp == True:
                            data_states_env_bp_kin, bounds_states_env_bp_kin = comp_statevector(data_FB_env_bp_kin)
                            data_states_env_bp_kurt, bounds_states_env_bp_kurt = comp_statevector(data_FB_env_bp_kurt)
                            if len(data_env_bp_kin) == 0:
                                data_env_bp_kin = np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))
                            else:
                                data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                            if len(data_env_bp_kurt) == 0:
                                data_env_bp_kurt = np.reshape(data_states_env_bp_kurt,(len(data_states_env_bp_kurt), 1))
                            else:
                                data_env_bp_kurt = np.concatenate((data_env_bp_kurt, np.reshape(data_states_env_bp_kurt,(len(data_states_env_bp_kurt), 1))), axis=1)
                row_names.append('sig' + str(i))
                icons['sig' + str(i)] = PIL.Image.open(path_icon + str(freqencies[i]) + '_orange.eps').resize(targetsize)

        elif code == 'your_own_signal':
            path = None
            kinematicfq = None
            signal, time, fs = load_data(path)
            freq, time_stft, stft, smag, _ = gen_mag_fb(signal, fs, windowlength, overlap_percent)
            freq_powerspectrum, spec_powerspectrum = welch(signal, fs, nperseg=windowlength, scaling='spectrum', noverlap=int(np.ceil(windowlength * overlap_percent)), window='hann')
            if envelope_comp == True:
                envelope = comp_envelope(signal, time, fs)
                freq_powerspectrum_env, spec_powerspectrum_env = welch(envelope, fs, nperseg=windowlength, scaling='density', noverlap=int(np.ceil(windowlength * overlap_percent)))
                freq_env, time_stft_env, stft_env, smag_env, _ = gen_mag_fb(envelope, fs, windowlength, overlap_percent)
            if bandpass == True:
                for kin in kinematicfq:
                    signal_filt_kin = bandpass_filter(signal, (1 - percent_filter) * kin, (1 + percent_filter) * kin, fs, 2)
                    if len(filtered_signal) == 0:
                        filtered_signal = np.reshape(signal_filt_kin, (len(signal_filt_kin), 1))
                    else:
                        filtered_signal = np.concatenate((filtered_signal, np.reshape(signal_filt_kin, (len(signal_filt_kin), 1))), axis=1)
                kinematic_filtered_signal = np.sum(filtered_signal, axis=1)
                freq_bp_kin, time_stft_bp_kin, stft_bp_kin, smag_bp_kin, _ = gen_mag_fb(kinematic_filtered_signal, fs, windowlength, overlap_percent)
                if envelope_comp == True:
                    envelope_bp_kin = comp_envelope(kinematic_filtered_signal, time, fs)
                    freq_powerspectrum_env_bp_kin, spec_powerspectrum_env_bp_kin = welch(envelope_bp_kin, fs, nperseg=windowlength, scaling='density', noverlap=int(np.ceil(windowlength * overlap_percent)))
                    freq_env_bp_kin, time_stft_env_bp_kin, stft_env_bp_kin, smag_env_bp_kin, _ = gen_mag_fb(envelope_bp_kin, fs, windowlength, overlap_percent)
            if plot_all == True:
                plot_signal(signal, time, xlab='Time (seconds)', title='Acceleration ' + ID, save=True)
                plot_signal_kin(kinematicfq, 20 * np.log10(spec_powerspectrum)[0:90], freq_powerspectrum[0:90], xlab='Frequency (Hz)', ylab='Power (dB)', title='Power Signal' + ID, save=True)
                plot_signal(smag, time=time_stft, freq=freq, xlab='Time', ylab='Frequency', colorbarlab='dB', title='Spectral Power', colmesh=True)
                if envelope_comp == True:
                    plot_signal(envelope, time, xlab='Time (seconds)', title='Envelope ' + ID, save=True)
                    plot_signal_kin(kinematicfq, spec_powerspectrum_env[0:700], freq_powerspectrum_env[0:700], xlab='Frequency (Hz)', ylab='Peak Amplitude', title='Power Envelope ' + ID, save=True)
                if bandpass == True:
                    plot_signal(kinematic_filtered_signal, time, xlab='Time (seconds)', title='Acceleration Bandpass Kinematic ' + ID, save=True)
                    if envelope_comp == True:
                        plot_signal(envelope_bp_kin, time, xlab='Time (seconds)', title='Envelope Bandpass AX ' + ID, save=True)
                        plot_signal_kin(kinematicfq, freq_powerspectrum_env_bp_kin, spec_powerspectrum_env_bp_kin, xlab='Frequency (Hz)', ylab='Peak Amplitude', title='Power Envelope Bandpass ' + ID, save=True)
            freq_range = np.ceil(freq[0:num_nodes + 1])
            for i in np.arange(len(freq_range) - 1):
                print('Frequency Band: ', freq_range[i], freq_range[i + 1])
                data_FBAX = smag[i, :]
                if envelope_comp == True:
                    data_FB_env = smag_env[i, :]
                if bandpass == True:
                    data_FB_bp_kin = smag_bp_kin[i, :]
                    if envelope_comp == True:
                        data_FB_env_bp_kin = smag_env_bp_kin[i, :]
                if use_baseline_case == True:
                    if ID == ID_list[0]:
                        data_states, bounds_states = comp_statevector(data_FB)
                        if len(bounds) == 0:
                            bounds = np.reshape(bounds_states, (len(bounds_states), 1))
                        else:
                            bounds = np.concatenate((bounds, np.reshape(bounds_states, (len(bounds_states), 1))), axis=1)
                        if len(data) == 0:
                            data = np.reshape(data_states, (len(data_states), 1))
                        else:
                            data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                        if envelope_comp == True:
                            data_states_env, bounds_states_env = comp_statevector(data_FB_env)
                            if len(bounds_env) == 0:
                                bounds_env = np.reshape(bounds_states_env, (len(bounds_states_env), 1))
                            else:
                                bounds_env = np.concatenate((bounds_env, np.reshape(bounds_states_env, (len(bounds_states_env), 1))), axis=1)
                            if len(data_env) == 0:
                                data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                            else:
                                data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                        if bandpass == True:
                            data_states_bp_kin, bounds_states_bp_kin = comp_statevector(data_FB_bp_kin)
                            if len(bounds_bp_kin) == 0:
                                bounds_bp_kin = np.reshape(bounds_states_bp_kin, (len(bounds_states_bp_kin), 1))
                            else:
                                bounds_bp_kin = np.concatenate((bounds_bp_kin, np.reshape(bounds_states_bp_kin, (len(bounds_states_bp_kin), 1))), axis=1)
                            if len(data_bp_kin) == 0:
                                data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                            else:
                                data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                            if envelope_comp == True:
                                data_states_env_bp_kin, bounds_states_env_bp_kin = comp_statevector(data_FB_env_bp_kin)
                                if len(bounds_env_bp_kin) == 0:
                                    bounds_env_bp_kin = np.reshape(bounds_states_env_bp_kin,(len(bounds_states_env_bp_kin), 1))
                                else:
                                    bounds_env_bp_kin = np.concatenate((bounds_env_bp_kin, np.reshape(bounds_states_env_bp_kin, (len(bounds_states_env_bp_kin), 1))), axis=1)
                                if len(data_env_bp_kin) == 0:
                                    data_env_bp_kin = np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))
                                else:
                                    data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                    else:
                        bounds_states = bounds[:, 2*i]
                        data_states = np.digitize(data_FB, bounds_states)
                        if len(data) == 0:
                            data = np.reshape(data_states, (len(data_states), 1))
                        else:
                            data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                        if envelope_comp == True:
                            bounds_states_env = bounds_env[:, 2 * i]
                            data_states_env = np.digitize(data_FB_env, bounds_states)
                            if len(data_env) == 0:
                                data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                            else:
                                data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                        if bandpass == True:
                            bounds_states_bp_kin = bounds_bp_kin[:, 2 * i]
                            data_states_bp_kinAX = np.digitize(data_FB_bp_kin, bounds_states_bp_kin)
                            if len(data_bp_kin) == 0:
                                data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                            else:
                                data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                            if envelope_comp == True:
                                bounds_states_env_bp_kin = bounds_bp_kin[:, 2 * i]
                                data_states_env_bp_kin = np.digitize(data_FB_env_bp_kin, bounds_states_env_bp_kin)
                                if len(data_env_bp_kin) == 0:
                                    data_env_bp_kin = np.reshape(data_states_env_bp_kin,(len(data_states_env_bp_kin), 1))
                                else:
                                    data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                else:
                    data_statesAX, bounds_statesAX = comp_statevector(data_FB)
                    if len(data) == 0:
                        data = np.reshape(data_states, (len(data_states), 1))
                    else:
                        data = np.concatenate((data, np.reshape(data_states, (len(data_states), 1))), axis=1)
                    if envelope_comp == True:
                        data_states_env, bounds_states_env = comp_statevector(data_FB_env)
                        if len(data_env) == 0:
                            data_env = np.reshape(data_states_env, (len(data_states_env), 1))
                        else:
                            data_env = np.concatenate((data_env, np.reshape(data_states_env, (len(data_states_env), 1))), axis=1)
                    if bandpass == True:
                        data_states_bp_kin, bounds_states_bp_kin = comp_statevector(data_FB_bp_kin)
                        if len(data_bp_kin) == 0:
                            data_bp_kin = np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))
                        else:
                            data_bp_kin = np.concatenate((data_bp_kin, np.reshape(data_states_bp_kin, (len(data_states_bp_kin), 1))), axis=1)
                        if envelope_comp == True:
                            data_states_env_bp_kin, bounds_states_env_bp_kin = comp_statevector(data_FB_env_bp_kin)
                            if len(data_env_bp_kin) == 0:
                                data_env_bp_kin = np.reshape(data_states_env_bp_kin,(len(data_states_env_bp_kin), 1))
                            else:
                                data_env_bp_kin = np.concatenate((data_env_bp_kin, np.reshape(data_states_env_bp_kin, (len(data_states_env_bp_kin), 1))), axis=1)
                row_names.append('signal' + str(i))
                icons['signal' + str(i)] = PIL.Image.open(path_icon + str(freqencies[i]) + '_orange.eps').resize(targetsize)

        spath = 'figure_graph/'
        dag, weights = generate_tree(data, row_names, cut_dataset, len_dataset, '_' + str(ID))
        if ID == ID_list[0]:
            pos = nx.circular_layout(dag)
        plot_graph(dag,weights,pos, icons, label=weights_bool, save_path=spath + str(ID), draw_grey_edges=draw_grey_edges)
        if envelope_comp == True:
            dag_env, weights_env = generate_tree(data_env, row_names, cut_dataset, len_dataset, '_envelope_' + str(ID))
            plot_graph(dag,weights_env,pos, icons, label=weights_bool, save_path=spath + str(ID) + '_envelope_', draw_grey_edges=draw_grey_edges)
        if bandpass == True:
            dag_bp_kin, weights_kin = generate_tree(data_bp_kin, row_names, cut_dataset, len_dataset, '_bp_kinematic_' + str(ID))
            plot_graph(dag_bp_kin,weights_kin,pos, icons, label=weights_bool, save_path=spath + str(ID) + '_bp_kinematic_', draw_grey_edges=draw_grey_edges)
            dag_bp_kurt, weights_kurt = generate_tree(data_bp_kurt, row_names, cut_dataset, len_dataset, '_bp_kurtosis_' + str(ID))
            plot_graph(dag_bp_kurt,weights_kurt,pos, icons, label=weights_bool, save_path=spath + str(ID) + '_bp_kurt_', draw_grey_edges=draw_grey_edges)
            if envelope_comp == True:
                dag_env_bp_kin, weights_env_kin = generate_tree(data_env_bp_kin, row_names, cut_dataset, len_dataset, '_envelope_bp_kinematic_' + str(ID))
                plot_graph(dag_env_bp_kin,weights_env_kin,pos, icons, label=weights_bool, save_path=spath + str(ID) + '_envelope_bp_kinematic_', draw_grey_edges=draw_grey_edges)
                dag_env_bp_kurt, weights_env_kurt = generate_tree(data_env_bp_kurt, row_names, cut_dataset, len_dataset,'_envelope_bp_kurtosis_' + str(ID))
                plot_graph(dag_env_bp_kurt,weights_env_kurt,pos, icons, label=weights_bool, save_path=spath + str(ID) + '_envelope_bp_kurt_', draw_grey_edges=draw_grey_edges)

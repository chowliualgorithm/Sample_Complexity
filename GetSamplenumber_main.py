"""
# ----------------------- Get Sample Number ------------------------- #
For a given threshold, confidence level and model type, this script
returns the upper bound and the empirical number of samples for
different number of nodes and states where the probability distribution
has to be given for each model. Alternatively it can also return the
upper bound and the empirical number of required samples to reconstruct
the structure of a given model such that the probability of an error
is smaller than a given threshold for different values of gamma, resp.
different distributions and level of confidence.
# ------------------------------------------------------------------- #

To run this script make sure the following packages are installed:
-numpy
-networkx
-scipy
-pgmpy
-matplotlib
-pandas

If the simulated rate has to be computed you need further
-multiprocessing
"""

from GetSamplenumber_intern_function import *

import warnings
warnings.filterwarnings('ignore')

if __name__ == '__main__':
    """
    define parameter and make sure all required data is available:
    threshold, number of nodes, number of states
    load probabilities such that for all number of states and nodes the error exponent K_p is almost equal
    take the smallest K_p as K_p for the upper bound form Tan, 2011
    """
    model_variation = False                  # set to true if variation in nodes and states
    given_model = not model_variation       # determine accuracy for given model and number of samples
    load_data = 'tree_nodes_8_states_4_threshold_0.001_epsilon_0.001_n' #                         # enter path to load data for plotting only  'samplenumberchain_nodes_3-8_states_2-5_threshold_0.001_epsilon_0.001_gamma_1.0'
    iterate_n = False                       # Computes n, otherwise only upper bound of Tan is returned
    load_dist_data = True                   # in case that model variation == True, Load gamma computed in GenerateDistribution_main.py to ensure Kp are all similar, data has to be available

    if load_data == None:
        # this parameter have to be defined for model_variation == True
        th = 1e-3                           # threshold for error probability
        diff_approx_true = 1e-3             # threshold for the difference between estimated probability and true probability
        model_type = 'chain'                 # 'chain', 'star' or 'tree'
        if model_variation == True:
            # this parameter have to be defined for model_variation == True
            conf_I = 0.95  # define a confidence interval
            gamma = 0.01   # define gamma to either generate a distribution or choose a distribution
            m = max(int(np.ceil(np.power(norm.ppf(conf_I) / diff_approx_true * np.sqrt((th+diff_approx_true) * (1 - (th+diff_approx_true))), 2))), np.ceil(6/(th*(1-th))))
            if load_dist_data == False:
                num_nodes = np.arange(3, 9)     # array of int defining the number of nodes
                num_states = np.arange(2, 6)    # array of defining the number of states
                result_tuples = list(product(num_nodes, num_states))
                # Caution, using gives n for different error exponents Kp!
                with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                    result = pool.map(get_n, [(ele[0], ele[1], m, None, th, diff_approx_true, model_type, gamma, conf_I, iterate_n) for ele in result_tuples])
                load_data = 'samplenumber' + model_type + '_nodes_' + str(np.min(num_nodes)) + '-' + str(np.max(num_nodes)) + '_states_' + str(np.min(num_states)) + '-' + str(np.max(num_states)) + '_threshold_' + str(th) + '_epsilon_' + str(diff_approx_true) + '.txt'
            else:
                data_Kp = pd.read_csv('data_GenerateDistribution/' + model_type + str(int(gamma*100)) + '_Kp.csv', index_col=0)  # make sure that the dataframe has columns: number of states, rows: number of nodes
                data_gamma = pd.read_csv('data_GenerateDistribution/' + model_type + str(int(gamma*100)) + '_gamma.csv', index_col=0)  # make sure that the dataframe is the same size as data_Kp
                num_nodes = np.array(data_Kp.index)
                num_states = np.array(data_Kp.columns)
                result_tuples = list(product(num_nodes, num_states))
                Kp = data_Kp.values.min()
                with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                    result = pool.map(get_n, [(int(ele[0]), int(ele[1]), m, Kp, th, diff_approx_true, model_type, data_gamma.loc[(ele[0], ele[1])], conf_I, iterate_n) for ele in result_tuples])
                load_data = 'samplenumber' + model_type + '_nodes_' + str(np.min(num_nodes)) + '-' + str(np.max(num_nodes)) + '_states_' + str(np.min(num_states)) + '-' + str(np.max(num_states)) + '_threshold_' + str(th) + '_epsilon_' + str(diff_approx_true) + '_gamma_' + str(gamma*100) +'.txt'
        elif given_model == True:
            # this parameter have to be defined for given_model == True
            num_nodes = 8                                   # int defining the number of nodes
            num_states = 4                                  # int defining the number of states
            conf_I = np.array([0.90, 0.95, 0.99, 0.999])    # considered confidence intervals
            gamma = np.array([0.01, 0.05, 0.1, 0.15, 0.2])  # or generate another distribution in GetSamplenumber_intern_funtions.py

            result_tuples = list(product(conf_I, gamma))
            with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
                result = pool.map(get_n, [(num_nodes, num_states, int(max(np.ceil(np.power(norm.ppf(ele[0]) / diff_approx_true * np.sqrt((th + diff_approx_true) * (1 - (th + diff_approx_true))), 2)),np.ceil(6/(th*(1-th))))), None, th,diff_approx_true, model_type, ele[1], ele[0], iterate_n) for ele in result_tuples])
            load_data = 'samplenumber' + model_type + '_nodes_' + str(num_nodes) + '_states_' + str(num_states) + '_threshold_' + str(th) + '_epsilon_' + str(diff_approx_true) + '.txt'

        with open('data_SampleNumber/'+ load_data, 'w') as file:
            file.write(str(result))
        file.close()
        if model_variation:
            if iterate_n:
                df = pd.DataFrame(result, columns=['Number of Nodes', 'Number of States', 'threshold (Tan)', 'threshold (real)'])
                df.set_index(['Number of Nodes', 'Number of States'], inplace=True)
                ub_n = df[['threshold (Tan)']].unstack().to_numpy()
                real_n = df[['threshold (real)']].unstack().to_numpy()
            else:
                df = pd.DataFrame(result, columns=['Number of Nodes', 'Number of States', 'threshold (Tan)'])
                df.set_index(['Number of Nodes', 'Number of States'], inplace=True)
                ub_n = df[['threshold (Tan)']].unstack().to_numpy()
        else:
            if iterate_n:
                df = pd.DataFrame(result, columns=['Conficence Interval', 'Gamma', 'threshold (Tan)', 'threshold (real)'])
                df.set_index(['Confidence Interval', 'Gamma'], inplace=True)
                ub_n = df[['threshold (Tan)']].unstack().to_numpy()
                real_n = df[['threshold (real)']].unstack().to_numpy()
            else:
                df = pd.DataFrame(result, columns=['Confidence Interval', 'Gamma', 'threshold (Tan)'])
                df.set_index(['Confidence Interval', 'Gamma'], inplace=True)
                ub_n = df[['threshold (Tan)']].unstack().to_numpy()
    else:
        load_data_path = 'data_SampleNumber/' + load_data + '.txt'
        with open(load_data_path, 'r') as file:
            lines = file.readlines()
        data = [eval(line.strip()) for line in lines]

        result = np.array(data[0])
        if "star" in load_data_path:
            model_type = 'star'
        elif "tree" in load_data_path:
            model_type = 'tree'
        elif "chain" in load_data_path:
            model_type = 'chain'

        match = re.search(r'threshold_([^_]+)', load_data_path)
        if match:
            th = match.group(1)
            print("Threshold:", th)
        else:
            print("Threshold not found in the file name.")

        match = re.search(r'epsilon_([^_]+)', load_data_path)
        if match:
            diff_approx_true = match.group(1)
            print("Epsilon:", diff_approx_true)
        else:
            print("Epsilon not found in the file name.")

        if "-" in load_data_path:
            if result.shape[1] == 4:
                df = pd.DataFrame(result, columns=['Number of Nodes', 'Number of States', 'threshold (Tan)', 'threshold (real)'])
                df.set_index(['Number of Nodes', 'Number of States'], inplace=True)
            elif result.shape[1] == 3:
                df = pd.DataFrame(result, columns=['Number of Nodes', 'Number of States', 'threshold (Tan)'])
                df.set_index(['Number of Nodes', 'Number of States'], inplace=True)
        else:
            if result.shape[1] == 4:
                df = pd.DataFrame(result, columns=['Confidence Interval', 'Gamma', 'threshold (Tan)', 'threshold (real)'])
                df.set_index(['Confidence Interval', 'Gamma'], inplace=True)
            elif result.shape[1] == 3:
                df = pd.DataFrame(result, columns=['Confidence Interval', 'Gamma', 'threshold (Tan)'])
                df.set_index(['Confidence Interval', 'Gamma'], inplace=True)

    save_file_as = 'figures_samplenumber/' + load_data
    plot_3D_n(df, save_file_as, model_type, th, diff_approx_true)
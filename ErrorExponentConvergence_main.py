"""
# --------------- Convergence of the Error Exponent ----------------- #
This script can be used to investigate the convergence of the empirical
error probability to the true error exponent for a given distribution.
The empirical probability is computed as #Error/num_runs where the
number of samples is fixed. As the error event is a rare event num_runs
has to be suitably high. This means the computational effort is very
high and therefore this method is not recommended for models with more
than 5 nodes.

For more information see the paper of Vincent Y. F. Tan, A
Large-Deviation Analysis of the Maximum-Likelihood Learning of Markov
Tree Structures, 2011, DOI: 10.1109/TIT.2011.2104513
# ------------------------------------------------------------------- #

To run this script make sure the following packages are installed:
-numpy
-networkx
-scipy
-pgmpy
-matplotlib

If the simulated rate has to be computed you need further
-multiprocessing
"""

from ErrorExponentConvergence_intern_functions import *

if __name__ == '__main__':

    # Parameter
    load = True                     # True to load data, False to compute convergence data

    #-------------------------------- generate model ---------------------------------#

    # Either generate a BayesianNetwork from pgmpy.models with given cpds
    # or generate a model by defining the following parameters

    gamma = 0.01                    # Data available for 0.2 and 0.01
    num_init = 200                  # number of initial values for the computation of the exact rate
    model_str = 'star'              # 'chain', 'star', 'tree'
    num_nodes = 7                   # number of nodes/random variables
    num_states = 2                  # number of states for each random variable
    model = get_complete_model(model_str,gamma, num_nodes, num_states)

    #------------------------------ compute exact rate -------------------------------#
    # compute the exact error rate from given model

    if model_str == 'star': # due to symmetry stars are faster to compute
        # set edges
        edge1 = tuple((0, 1))
        edge2 = tuple((1, 2))

        exact_rate = get_exact_rate_star(model, edge1, edge2, num_init)
    else:
        exact_rate,_,_ = get_exact_rate(model, num_init)

    #----------------------------- load data or simulate ------------------------------#

    if load == True:
        path = 'data_ErrorExponentConvergence/'+ model_str + str(num_nodes) + 'gamma' + str(int(gamma*100)) + '.txt'
        res = np.loadtxt(path)
    else:
        # Caution, multiprocessing is used.
        num_runs = 1000      # number of runs to compute average error probability
        num_samples = np.arange(int(10), int(60), int(10))    # numpy array with number of samples that should be examined
        num_cpus = 4                                            # if not defined then the available cpus are counted
        save_result_path = 'data_ErrorExponentConvergence/'+ model_str + str(num_nodes) + 'gamma' + str(int(gamma*100)) + 'test'+'.txt'
        res = np.array(get_simulated_rate(model, num_runs, num_samples, save_result_path, num_cpus)) # add num_cpus if you want to use less than available

    # ---------------------------------- plot data ------------------------------------#

    num_samples = res[:,0]
    sim_rate = res[:,2]
    colors = {'blue': '#4C72B0',
            'orange': '#DD8452',
            'green': '#55A868',
            'red': '#C44E52',
            'purple': '#8172B3'}
    path_fig = 'figures_ErrorExponentConvergence/'
    plot_rate_sim_exact(model_str, num_nodes, gamma, np.array(num_samples), exact_rate * np.ones((len(num_samples),)), sim_rate, colors, path_fig)

import numpy as np
import networkx as nx
import multiprocessing
import time

import matplotlib.pyplot as plt
from matplotlib import font_manager

from datetime import timedelta

from scipy.stats import entropy
from scipy.optimize import minimize, LinearConstraint, NonlinearConstraint

from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianNetwork
from pgmpy.factors.discrete.CPD import TabularCPD
from pgmpy.estimators import TreeSearch
from pgmpy.sampling import BayesianModelSampling

import warnings
warnings.filterwarnings('ignore')

def get_exact_rate_star(model, edge1, edge2, num_init_values_eR):
    """
        returns the exact error rate for a star graph model where the distribution of the true edges are equal
        this is only for faster computation. For arbitrary graphs use get_exact_rate

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        num_init_values_eR: integer, defines the number of different starting points for the nonlinear optimization problem
    """
    inference = VariableElimination(model)
    joint_dist_Pee = inference.query(variables=list(sorted(set(edge1 + edge2), key=list(edge1 + edge2).index)))

    exact_rate = 10

    for i in range(num_init_values_eR):
        initial_guess_temp = np.random.random(joint_dist_Pee.values.shape)
        initial_guess_temp = initial_guess_temp / np.sum(initial_guess_temp)

        exact_rate_temp, _ = exact_Jee(model, edge1, edge2, initial_guess_temp)

        if exact_rate_temp < exact_rate:
            exact_rate = exact_rate_temp

    return exact_rate

def get_exact_rate(model, num_init_values_eR):
    """
        returns the exact error rate for an arbitrary given model

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        num_init_values_eR: integer, defines the number of different starting points for the nonlinear optimization problem
    """
    # set edges
    exact_rate = 10
    e_inP = None
    e_ninP = None

    edge_prime = nx.complement(model.to_undirected()).edges
    for e_prime in edge_prime:
        path = nx.shortest_path(model.to_undirected(), source=e_prime[0], target=e_prime[1])
        for i in range(len(path) - 1):
            e = tuple((path[i], path[i + 1]))
            inference = VariableElimination(model)
            joint_dist_Pee = inference.query(variables=list(sorted(set(e + e_prime), key=list(e + e_prime).index)))
            for j in range(num_init_values_eR):
                initial_guess_temp = np.random.random(joint_dist_Pee.values.shape)
                initial_guess_temp = initial_guess_temp / np.sum(initial_guess_temp)

                exact_rate_temp, joint_dist_Q_temp = exact_Jee(model, e, e_prime, initial_guess_temp)

                if exact_rate_temp < exact_rate:
                    exact_rate = exact_rate_temp
                    e_inP = e
                    e_ninP = e_prime
    return exact_rate, e_inP, e_ninP

def mutual_info(model, edge):
    """
    returns the mutual information of the two variables of the given edge

    Parameters
    ----------
    model: BayesianNetwork from pgmpy.models
        cpd have to be given

    edge: tuple with the two nodes for which the mutual information has to be computed
    """
    # Compute Joint Distribution
    inference = VariableElimination(model)
    joint_dist_Pe = inference.query(variables=list(edge))
    marg_dist_e0 = inference.query(variables=[edge[0]])
    marg_dist_e1 = inference.query(variables=[edge[1]])

    mutual_info = 0
    for x in range(model.get_cpds(edge[0]).variable_card):
        for y in range(model.get_cpds(edge[0]).variable_card):
            mutual_info += joint_dist_Pe.values[x, y] * np.log(joint_dist_Pe.values[x, y] / (marg_dist_e0.values[x] * marg_dist_e1.values[y]))
    return mutual_info

def approx_Jee(model, edge1, edge2):
    """
    returns the approximate rate of two nodes e, e': J_e,e' = (E(s_e'-s_e)^2/(2*Var(s_e' - s_e))

    Parameters
    ----------
    model: BayesianNetwork from pgmpy.models
        cpd have to be given

    edges: tuples with the two nodes for which the mutual information has to be computed, e is in E_P and e' is not in E_P
    """
    # switch edges in the case that edge2 is contained in path of edge1 (therefore edge2 is not contained in E_P
    if edge2[0] in nx.all_simple_paths(model, source=edge1[0], target=edge1[1]) and not set(edge1).intersection(set(edge2)):
        dummy = edge1
        edge1 = edge2
        edge2 = dummy
    # switch order of edges such that the shared node is in the middle
    if len(set(edge1+edge2)) == 3 and edge1[0] in edge2:
        edge1 = edge1[::-1]
    if len(set(edge1+edge2)) == 3 and edge2[1] in edge1:
        edge2 = edge2[::-1]

    inference = VariableElimination(model)
    joint_dist_Pee = inference.query(variables=list(sorted(set(edge1+edge2), key=list(edge1+edge2).index)))
    mutual_information_edge1 = mutual_info(model, edge1)
    mutual_information_edge2 = mutual_info(model, edge2)

    diff_MI = (mutual_information_edge1-mutual_information_edge2)**2

    inf_dens_edge1 = information_density(model, edge1)
    inf_dens_edge2 = information_density(model, edge2)

    # Var(X) = E(X^2)-E(X)^2
    var_info_dens = 0

    if len(set(edge1+edge2)) == 3:
        for i in range(joint_dist_Pee.cardinality[0]):
            for j in range(joint_dist_Pee.cardinality[1]):
                for k in range(joint_dist_Pee.cardinality[2]):
                    var_info_dens += joint_dist_Pee.values[i, j, k] * (inf_dens_edge2[j, k] - inf_dens_edge1[i, j]) ** 2
    else:
        for i in range(joint_dist_Pee.cardinality[0]):
            for j in range(joint_dist_Pee.cardinality[1]):
                for k in range(joint_dist_Pee.cardinality[2]):
                    for l in range(joint_dist_Pee.cardinality[3]):
                        var_info_dens += joint_dist_Pee.values[i, j, k, l] * (inf_dens_edge2[k, l] - inf_dens_edge1[i, j]) ** 2

    J_ee = diff_MI/(2*(var_info_dens-diff_MI))

    return J_ee, joint_dist_Pee

def information_density(model, edge):
    """
    returns the information density of two random variables X,Y: s(x,y) = log(P(x,y)/(P(x)*P(y)))

    Parameters
    ----------
    model: BayesianNetwork from pgmpy.models
        cpd have to be given

    edge: tuples with the two nodes for which the mutual information has to be computed
    """
    inference = VariableElimination(model)
    joint_dist = inference.query(variables=list(edge))
    marg_dist_e0 = inference.query(variables=[edge[0]])
    marg_dist_e1 = inference.query(variables=[edge[1]])
    information_density = np.log(joint_dist.values/marg_dist_e0.values.reshape((2,1))/marg_dist_e1.values.reshape((1,2)))
    return information_density

def exact_Jee(model, edge1, edge2, initial_guess):
    """
        returns the error exponent of a crossover event for two edges computed as a solution of the
        nonlinear-optimization problem: min_Q KL(Q||P_{e,e'}) with condition MI(Q_e) = MI(Q_e')

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        edge1/edge2: two possible edges in the model, one has to be in the edge set of the true distribution, the other one not

        initial_guess: initial guess for the nonlinear_optimization problem
    """
    # switch edges in the case that edge2 is contained in path of edge1 (therefore edge2 is not contained in E_P
    pathset = []
    for path in nx.all_simple_paths(model, source=edge1[0], target=edge1[1]):
        pathset = set(pathset + path)
    if edge2[0] in pathset and not set(edge1).intersection(set(edge2)):
        dummy = edge1
        edge1 = edge2
        edge2 = dummy
    # switch order of edges such that the shared node is in the middle
    if len(set(edge1 + edge2)) == 3 and edge1[0] in edge2:
        edge1 = edge1[::-1]
    if len(set(edge1 + edge2)) == 3 and edge2[1] in edge1:
        edge2 = edge2[::-1]

    inference = VariableElimination(model)
    joint_dist = inference.query(variables=list(sorted(set(edge1 + edge2), key=list(edge1 + edge2).index)))
    Pee = joint_dist.values.flatten()
    def objective(Q, P):
        return entropy(Q, P)

    valid_distribution_constraint_1 = LinearConstraint(np.ones((len(Pee),)), 1, 1)
    valid_distribution_constraint_2 = LinearConstraint(np.eye((len(Pee))), 0, 1)

    if len(joint_dist.values.shape) == 3:
        def constraint_MI_3(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=2)
            Qeprime = np.sum(Qee, axis=0)

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]), 1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_3, 0, 0)
    else:
        def constraint_MI_4(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=(2, 3))
            Qeprime = np.sum(Qee, axis=(0, 1))

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]),1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_4, 0, 0)

    options = {'maxiter': 1e6,  # Maximum number of iterations
               'disp': False}   # Display convergence messages

    method='SLSQP'

    result = minimize(lambda Q: objective(Q, Pee), initial_guess.flatten(), constraints=[valid_distribution_constraint_1, valid_distribution_constraint_2, MI_constraint], method=method,options=options)

    return result.fun, np.reshape(result.x, joint_dist.values.shape)

# --------------------------- Compute Probability ---------------------------- #

def get_P1(num):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states
    """
    denom = sum(2**i for i in range(num))
    prob = np.reshape(np.array(2**np.arange(num)/denom), (num,1))
    return prob.tolist()

def get_Pi(num, gamma):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states

    gamma: parameter influencing the distance to the uniform distribution
    """
    g = gamma
    mat = np.ones((num,num))/num - np.ones((num,num))*g/(num-1) + np.eye(num)*(1 + 1/(num-1))*g
    return mat.tolist()

# ----------------------------- Generate Model ------------------------------- #

def get_complete_model(model_type, gamma, num_nodes, num_states):
    """
    returns a Bayesian Network with a given structure and the distribution is defined by get_P1() and get_Pi()

    Parameters
    ----------
    model_type: type of the model, either 'tree' for a binary tree 'star' or 'chain' possible

    gamma: parameter influencing the distance to the uniform distribution

    num_nodes: nodes the model should have

    num_states: number of possible states per node
    """
    tree = nx.DiGraph()
    edges = []

    if model_type == 'tree':
        for num in np.arange(1, num_nodes):
            if num % 2 == 1:
                edges.append(((num-1)/2, num))
            else:
                edges.append(((num - 2) / 2, num))
    elif model_type == 'star':
        for num in np.arange(1, num_nodes):
            edges.append((0, num))
    elif model_type == 'chain':
        for num in np.arange(1, num_nodes):
            edges.append((num - 1, num))

    tree.add_edges_from(edges)
    model_tree = BayesianNetwork(tree)
    cpd = TabularCPD(0, variable_card=num_states, values=get_P1(num_states))
    model_tree.add_cpds(cpd)
    mat = get_Pi(num_states, gamma)
    for e in model_tree.to_undirected().edges:
        if e[0] < e[1]:
            cpd = TabularCPD(e[1], variable_card=num_states, values=mat, evidence=[e[0]],
                             evidence_card=[num_states])
        else:
            cpd = TabularCPD(e[0], variable_card=num_states, values=mat, evidence=[e[1]],
                             evidence_card=[num_states])
        model_tree.add_cpds(cpd)
    return model_tree

# --------------------- get simulated rate ---------------------- #
def get_simulated_rate(model, num_runs, num_samples, path, num_cpus = multiprocessing.cpu_count()):
    """
        generates data from the model and reconstructs the structure of the model, counting the errors gives an approximation
        for the error probability (CLT)

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        num_runs: integer how often the model should be regenerated to estimate error probability

        num_samples: integer how many datapoints are used for reconstruction in every run

        path: here the data is saved

        num_cpus: integer number of cpus used for multiprocessing
    """
    print("Number of CPUs used:", num_cpus)
    all_samples = []
    start_time = time.time()
    for num in num_samples:
        print("Started loop with", num, "samples")
        with multiprocessing.Pool(processes=num_cpus) as pool:
            result = pool.map(worker, [(num, model, int(np.ceil(num_runs / num_cpus))) for _ in range(num_cpus)])
            num_error = np.sum(result)
            error_prob = num_error / (np.ceil(num_runs / num_cpus) * num_cpus)
            sim = -np.log(error_prob) / num
            all_samples.append(tuple((num, error_prob, sim)))
            np.savetxt(path,all_samples)
        print("Ended loop with", num, "samples")
        end_time = time.time()
        print("Time until now:", timedelta(seconds=round(end_time - start_time)))
        print("Estimated Time:", timedelta(seconds=round((end_time - start_time) * (len(num_samples) / (np.where(num_samples == num)[0][0] + 1) - 1))))
    return all_samples

def worker(*args):
    """
        interal function for multiprocessing
        for details see get_simulated_rate
    """
    num_samples, model, num_runs = args[0]
    iterator = range(num_runs)
    result = [get_error(num_samples, model) for _ in iterator]
    num_error = np.sum(result)
    return num_error

def get_error(num_samples, model):
    """
        returns 1 if an error event occured and 0 if the algorithm returned the model correctly from a given number of samples

        Parameters
        ----------
        num_samples: integer defining the number of samples used for reconstruction

        model: BayesianNetwork from pgmpy.models
            cpd have to be given
    """
    # ----------------------------- generate samples ----------------------------- #
    # sample data from BN tree structured
    inference_tree = BayesianModelSampling(model)
    df_data_tree = inference_tree.forward_sample(size=num_samples, show_progress=False)

    # ----------------------------- Chow Liu Algorithm --------------------------- #
    est = TreeSearch(df_data_tree)
    dag = est.estimate(estimator_type="chow-liu", show_progress=False)

    edgeset_true = set(model.to_undirected().edges)
    edgeset_CL = set(dag.to_undirected().edges)
    for el in edgeset_CL:
        if el not in edgeset_true and (el[1],el[0]) not in edgeset_true:
            return 1
    return 0


# ------------------- plot simulation rate ---------------------- #
def plot_rate_sim_exact(model_str, num_nodes, gamma, num_samples, exact_rate, sim_rate, color, path):
    """
    returns a plot with exact and approximate rate over the difference between mutual information

    Parameters
    ----------
    gamma: parameter to vary the difference between mutual information (only for name of the plots)

    num_samples: numpy array with the number of samples used for reconstruction (x-Axis)

    exact_rate: numpy array with the values for the exact rate, this is a constant over the number of samples

    sim_rate: numpy array with the values for the simulated rate given by 1/num_samples*log(#errors/M)

    colors: dictionary with colors
    """

    font_path = 'Add_Path/.../Fonts/LatinmodernmathRegular-z8EBa.otf'  # Your font path goes here
    font_manager.fontManager.addfont(font_path)
    prop = font_manager.FontProperties(fname=font_path)

    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = prop.get_name()
    plt.rcParams["font.size"] = "18"
    print(prop.get_name())

    fig, ax = plt.subplots()

    ax.plot(num_samples, exact_rate, '-', color=color['green'], label='Exact Rate')
    ax.plot(num_samples, sim_rate, '.-', color=color['blue'], label='Simulated Rate')
    ax.set_xlabel('Number of Samples')
    ax.set_ylabel('Error Exponent')
    if gamma == 0.01:
        if model_str == 'star':
            if num_nodes == 4:
                ax.set_ylim(5.5*1e-5, 9.5*1e-5)
            elif num_nodes == 7:
                ax.set_ylim(0, 9 * 1e-5)
        elif model_str == 'chain':
            if num_nodes == 4:
                ax.set_ylim(5.5*1e-5, 9.5*1e-5)
            elif num_nodes == 7:
                ax.set_ylim(0, 9.5 * 1e-5)
        elif model_str == 'tree':
            ax.set_ylim(0, 9.5 * 1e-5)
    if gamma == 0.2:
        if model_str == 'star':
            if num_nodes == 4:
                ax.set_ylim(0.02, 0.0325)
            elif num_nodes == 7:
                ax.set_ylim(0, 0.0325)
        elif model_str == 'chain':
            if num_nodes == 4:
                ax.set_ylim(0.02, 0.0325)
            elif num_nodes == 7:
                ax.set_ylim(0.01, 0.03)
        elif model_str == 'tree':
            ax.set_ylim(0.01, 0.03)


    # ax.grid(color='1')
    ax.legend()
    # fig.tight_layout()
    # ax.set_title('Comparsion Exact and Simulated Rates')

    fig.savefig(path + 'comp_sim_exact_' + model_str + str(num_nodes) + str(int(gamma * 100)) + '.png', bbox_inches="tight")
    fig.savefig(path + 'comp_sim_exact_' + model_str + str(num_nodes) + str(int(gamma * 100)) + '.eps', bbox_inches="tight")

    plt.show()
    return
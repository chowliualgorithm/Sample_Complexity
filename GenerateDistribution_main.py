"""
# ----------------------- Generate Distribution ---------------------- #
This script can be used to compute gamma such that the error exponent
of the different models is similar for better comparability in the next
step where the required sample number is computed see SampleNumber_main
and SampleNumber_intern_functions. This script can generate a set of
parameters for 'star', 'tree' and 'chain' graphs with a given type of
distribution for different numbers of nodes and states. If another
distribution or model type is required or other parameter have to be
changed one has to adapt the following.

The idea for the conditional distribution is from the paper of Vincent
Y. F. Tan, A Large-Deviation Analysis of the Maximum-Likelihood Learning
of Markov Tree Structures, 2011, DOI: 10.1109/TIT.2011.2104513
# ------------------------------------------------------------------- #
To run this script make sure the following packages are installed:
-numpy
-networkx
-scipy
-pgmpy
-itertools
-pandas
-multiprocessing
"""

from GenerateDistribution_intern_functions import *

import warnings
warnings.filterwarnings('ignore')

if __name__ == '__main__':
    num_states = np.arange(2, 4)        # possible number of states of each random variable
    num_nodes = np.arange(3, 4)         # possible number of nodes in the model
    result_tuples = list(product(num_nodes, num_states))

    # define parameter for the distribution
    gamma = 0.2                         # define gamma for the template model
    gamma_sp = gamma                    # define starting point of gamma for the optimization problem
    num_init = 25                       # number of initial values for the computation of the true error exponent
    model_type = 'star'                 # 'star', 'tree' or 'chain'

    # -------------------------------- generate model ---------------------------------#
    model_tree_comp = get_complete_model(model_type, gamma, np.min(num_nodes), np.min(num_states))
    exact_rate_comp, _, _ = get_exact_rate(model_tree_comp, num_init)

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        result = pool.map(comp_gamma, [(ele, gamma_sp, num_init, exact_rate_comp, model_type) for ele in result_tuples])

    file_path = 'data_GenerateDistribution/distribution_'+ model_type + str(int(gamma*100)) + '.txt'
    with open(file_path, 'w') as file:
        file.write(str(result))
        file.close()

    df = pd.DataFrame(result, columns=['num_nodes', 'num_states', 'gamma', 'Kp'])
    df.set_index(['num_nodes', 'num_states'], inplace=True)
    gamma_data = df['gamma'].unstack()
    Kp_data = df['Kp'].unstack()
    gamma_data.index.name = None
    gamma_data.columns.name = None
    Kp_data.index.name = None
    Kp_data.columns.name = None

    gamma_data.to_csv('data_GenerateDistribution/' + model_type + str(int(gamma*100)) + '_gamma.csv', index=True)
    Kp_data.to_csv('data_GenerateDistribution/' + model_type + str(int(gamma*100)) + '_Kp.csv', index=True)
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import scipy.io
import PIL
import csv

from scipy.signal import stft, windows, welch, convolve2d
from scipy.signal import hilbert
from scipy.stats import norm
from pgmpy.estimators import TreeSearch
from pgmpy.inference import VariableElimination
from matplotlib import font_manager
from scipy.signal import firwin, butter, lfilter

colors = {
    0: 'blue',
    1: 'orange',
    2: 'green',
    3: 'red',
    4: 'purple'
}

freqencies = {
    0: 1,
    1: 2,
    2: 4,
    3: 6,
    4: 8,
    5: 10,
    6: 14,
    7: 20,
    8: 28
}

def load_data(path):
    'TODO: add your code to load data as required'

    signal = None
    time = None
    fs = 1 / (time[1] - time[0])
    print('Frequency is ' + str(fs))

    return signal, time, fs

def load_dataML(path, ID):
    """
        load data from dataset given in https://www.mfpt.org/fault-data-sets/

        Parameters
        ----------
        path:   data path where the data is saved

        ID:     Element indicating the dataset that should be loaded
    """
    if "Baseline" in ID:
        number = ''.join(filter(str.isdigit, ID))
        path = path + '1 - Three Baseline Conditions/baseline_' + number + '.mat'
        case = np.array([0, 1, 2, 3])
    elif "Inner" in ID:
        number = ''.join(filter(str.isdigit, ID))
        path = path + '4 - Seven Inner Race Fault Conditions/InnerRaceFault_vload_' + number + '.mat'
        case = np.array([3, 2, 1, 0])
    elif "Outer" in ID:
        number = ''.join(filter(str.isdigit, ID))
        if int(number) < 4:
            path = path + '2 - Three Outer Race Fault Conditions/OuterRaceFault_' + number + '.mat'
            case = np.array([3, 2, 1, 0])
        else:
            number = str(int(number)-3)
            path = path + '3 - Seven More Outer Race Fault Conditions/OuterRaceFault_vload_' + number + '.mat'
            case = np.array([3, 2, 1, 0])


    signaldata = scipy.io.loadmat(path)['bearing'][0][0]
    fs = signaldata[case[0]][0][0]
    signal = signaldata[case[1]][:,0]
    if len(signaldata[case[2]].shape) > 1:
        load = signaldata[case[2]][0,0]
    else:
        load = float(signaldata[case[2]][0])
    time = np.array([i/fs for i in range(len(signal))])
    inputshaftrate = signaldata[case[3]][0,0]
    rd = 0.235
    pd = 1.245
    ne = 8
    angle = 0
    dia = rd/pd*np.cos(angle)
    kinematicfreq = np.array([inputshaftrate, ne*inputshaftrate/2*(1-dia), ne*inputshaftrate/2*(1+dia), inputshaftrate/2*(1-dia**2), inputshaftrate*pd/2/rd*(1-dia**2)])
    if fs > 97650:
        signal = signal[::2]
        time = time[::2]
        fs = fs/2
    print('Frequency is ' + str(fs))
    return signal, time, kinematicfreq, fs, load

def gen_mag_fb(signal, fs, wl, ol_perc, usehann = True):
    """
        Compute the short-time-fourier-transform (stft) of a given signal where the window is multiplied with the hahn window

        Parameters
        ----------
        signal:    signal data as np.array

        fs:        sampling frequency

        wl:        windowlength, int (usually a power of 2)

        ol_perc:   the overlap in percent (value between 0 and 100)
    """
    if usehann == True:
        window = windows.hann(wl)
        overlap_length = int(np.ceil(ol_perc*wl))
        freq, times, stft_data = stft(signal, fs=fs, window=window, nperseg=wl, noverlap=overlap_length, nfft=wl, boundary=None, padded=False)

        scale = 1 / np.sum(window)
        Zxx = stft_data / scale

        smag = 20 * np.log10(np.abs(Zxx))
        Zxx_db = np.sum(smag, 1)
    else:
        overlap_length = int(np.ceil(ol_perc * wl))
        freq, times, stft_data = stft(signal, fs=fs, nperseg=wl, noverlap=overlap_length)
    return freq, times, stft_data, smag, Zxx_db

def butter_bandpass_filter(data, lb, ub, fs, order=5):
    """
        classical bandpass filter for a given signal

        Parameters
        ----------
        data:    data as numpy array

        lb:      lower frequency bound for the bandpass filter

        ub:      upper frequency bound for the bandpass filter#

        fs:      sampling frequency of the signal

        order:   order of the butterworth filter, default is 5
    """
    b, a = butter(order, [lb, ub], fs=fs, btype='band')
    y = lfilter(b, a, data)
    return y

def bandpass_filter(data, lb, ub, fs, ntaps=128):
    """
        bandpass FIR filter for a given signal

        Parameters
        ----------
        data:    data as numpy array

        lb:      lower frequency bound for the bandpass filter

        ub:      upper frequency bound for the bandpass filter#

        fs:      sampling frequency of the signal

        ntaps:   windowsize?
    """

    taps_hamming = firwin(ntaps, [lb, ub], fs=fs, pass_zero=False, window='hamming', scale=False)
    y = lfilter(taps_hamming, 1.0, data)
    return y
def comp_envelope(sig, time, fs):
    """
        returns the hilbert transform of a given signal

        Parameters
        ----------
        sig:    signal as np.array
    """
    env = np.abs(hilbert(sig))

    return env
def approx_nd(data):
    """
        Calculate mean and standard deviation of a dataset

        Parameters
        ----------
        data:    dataset as np.array
    """
    mean = np.mean(data)
    std_dev = np.std(data)
    return mean, std_dev

def comp_statevector(data):
    """
        discretize the state space of each random variable
        for this we assume that the values of the magnitude of each frequency band is normal distributed. Hence we use
        for x states the 100/x quantiles to discretize the continuous data into x states

        Parameters
        ----------
        data:    data as numpy array
    """
    mean, std = approx_nd(data)
    dist = norm(mean, std)
    val25 = dist.ppf(0.25)
    val75 = dist.ppf(0.75)
    bounds = np.array([val25, mean, val75])
    data_states = np.digitize(data, bounds)
    return data_states, bounds

def generate_tree(data, row_names, cut_dataset, len_dataset, name):
    """
        generate a tree sturcutured graph from a dataset of length len_dataset, if cut_dataset is False, then the whole set is used.

        Parameters
        ----------
        data:    data as numpy array

        row_names: list of row names

        cut_dataset: bool, true if dataset should be cut

        len_dataset: length of the dataset that should be used for the reconstruction

        name: name for saving the weights

    """

    df_data = pd.DataFrame(data, columns=row_names)
    if cut_dataset == True:
        df_data = df_data[:len_dataset]
    print('Length Dataset: ', len(df_data))
    est = TreeSearch(df_data)#, root_node='sigA0')
    dag = est.estimate(estimator_type="chow-liu", edge_weights_fn='mutual_info', show_progress=False)
    weights = est._get_weights(df_data, edge_weights_fn='mutual_info')
    df_weights = pd.DataFrame(weights, columns=row_names, index=row_names)
    text_table = df_weights.to_latex(index=False)
    with open('weights/weights_' + name + '.txt', 'w') as file:
        file.write(text_table)
        file.close()
    return dag, df_weights
def plot_signal(sig, time = None, freq = None, xlab = None, ylab = None, colorbarlab = None, title = None, colmesh = False, save = False):
    """
        plot a signal

        Parameters
        ----------
        sig:    signal data as numpy array

        time:   time vector of signal data as numpy array

        freq:   frequency domain of the signal as numpy array

        xlab:   Label x-Axis

        ylab:   Label y-Axis

        colorbarlab: Label Colorbar

        title:  str title of the plot

        colmesh: Default is False, if True there is variance in 3 directions (then freq is not None)

        save:   bool, if True the plot is saved
    """

    font_path = 'Add_path/.../Fonts/LatinmodernmathRegular-z8EBa.otf'  # Your font path goes here
    font_manager.fontManager.addfont(font_path)
    prop = font_manager.FontProperties(fname=font_path)

    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = prop.get_name()
    plt.rcParams["font.size"] = "22"

    col = getcolors()
    plt.figure()
    if colmesh == True:
        if np.any(time) == None or np.any(freq) == None:
            print('Data missing!')
            return
        else:
            plt.pcolormesh(time, freq, sig, shading='auto')
            if colorbarlab != None:
                plt.colorbar(label=colorbarlab)
    else:
        if np.any(time) == None:
            plt.plot(sig, color=col['blue'])
        else:
            plt.plot(time, sig)
    if xlab != None:
        plt.xlabel(xlab)
    if ylab != None:
        plt.ylabel(ylab)
    if title != None:
        plt.title(title)
    plt.tight_layout()

    if save == True:
        save_path = 'figure_graph/'
        if title == None and xlab != None:
            title = xlab
        plt.savefig(save_path + title + '.png')
        plt.savefig(save_path + title + '.eps')
    else:
        print('Could not save Plot since title is missing!')

    plt.show(block=False)
    return

def plot_signal_kin(kin_freq, sig, time = None, freq = None, xlab = None, ylab = None, colorbarlab = None, title = None, colmesh = False, save = False):
    """
        plot a signal with vertical lines indicating the kinematic frequencies and its multiples

        Parameters
        ----------
        kin_freq: list or array containing the kinematic frequencies

        sig:    signal data as numpy array

        time:   time vector of signal data as numpy array

        freq:   frequency domain of the signal as numpy array

        xlab:   Label x-Axis

        ylab:   Label y-Axis

        colorbarlab: Label Colorbar

        title:  str title of the plot

        colmesh: Default is False, if True there is variance in 3 directions (then freq is not None)

        save:   bool, if True the plot is saved
    """
    font_path = 'Add_Path/.../Fonts/LatinmodernmathRegular-z8EBa.otf'  # Your font path goes here
    font_manager.fontManager.addfont(font_path)
    prop = font_manager.FontProperties(fname=font_path)

    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = prop.get_name()
    plt.rcParams["font.size"] = "22"

    if "Outer" in title:
        k = 0
    else:
        k = 1
    points = np.arange(0,9)*kin_freq[k]

    col = getcolors()
    plt.figure()
    if colmesh == True:
        if np.any(time) == None or np.any(freq) == None:
            print('Data missing!')
            return
        else:
            plt.pcolormesh(time, freq, sig, shading='auto')
            if colorbarlab != None:
                plt.colorbar(label=colorbarlab)
    else:
        if np.any(time) == None:
            plt.plot(sig, color=col['blue'])
        else:
            plt.plot(time, sig)
    if xlab != None:
        plt.xlabel(xlab)
    if ylab != None:
        plt.ylabel(ylab)
    if title != None:
        plt.title(title)

    for point in points:
        plt.axvline(x=point, color='grey', linestyle='dotted')
    plt.tight_layout()

    if save == True:
        save_path = 'figure_graph/'
        if title == None and xlab != None:
            title = xlab
        plt.savefig(save_path + title + '.png')
        plt.savefig(save_path + title + '.eps')
    else:
        print('Could not save Plot since title is missing!')

    plt.show(block=False)
    return

def plot_graph(G, weight,pos, icons, label, save_path, draw_grey_edges):
    """
        plot a signal with vertical lines indicating the kinematic frequencies and its multiples

        Parameters
        ----------
        G: networkX graph structure

        weight: weights between the nodes given as numpy array

        pos:    dictionary containing the positions of the nodes

        icons:  dictionary containing the icons for the nodes

        label:  bool, if True the edge weights are plotted

        save_path: path where the figure should be saved

        draw_grey_edges: bool, indicating if the complete graph should be plotted where the edges not chosen by the algorithm are grey
    """

    font_path = 'Add_Path/.../Fonts/LatinmodernmathRegular-z8EBa.otf'  # Your font path goes here
    font_manager.fontManager.addfont(font_path)
    prop = font_manager.FontProperties(fname=font_path)

    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = prop.get_name()
    plt.rcParams["font.size"] = "22"
    print(prop.get_name())


    edgelist = G.edges
    if label == True:
        for e in edgelist:
            G[e[0]][e[1]]['weight'] = weight[e[0]][e[1]].round(4)
    G = G.to_undirected()
    complement_edges = nx.complement(G).edges
    G_comp = nx.Graph()
    G_comp.add_edges_from(list(complement_edges))

    small_diff = 10
    for e in list(complement_edges):
        path = nx.shortest_path(G, source=e[0], target=e[1])
        for i in range(len(path) - 1):
            diff = np.abs(weight[e[0]][e[1]] - weight[path[i]][path[i + 1]])
            if diff < small_diff:
                small_diff = diff
    print(small_diff)

    # Plot the graph
    fig, ax = plt.subplots()


    nx.draw_networkx_nodes(G, pos, label=True, node_size=750, node_color='white', linewidths=1, edgecolors='#808080')
    if draw_grey_edges == True:
        nx.draw_networkx_edges(G_comp, pos, ax=ax, edge_color='#dddddd', arrows=False, label=True)

    nx.draw_networkx_edges(G, pos, ax=ax, edge_color='#808080', arrows=False, label=True)

    if label == True:
        edge_labels = nx.get_edge_attributes(G, 'weight')
        nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, label_pos=0.5, font_color='#808080', font_size=16, font_family='sans-serif', bbox = dict(boxstyle="square,pad=0.2", fc="white", ec="white"))#horizontalalignment='left', verticalalignment='bottom',)

    tr_figure = ax.transData.transform
    tr_axes = fig.transFigure.inverted().transform
    ax.axis('off')

    # Select the size of the image (relative to the X axis)
    icon_size = (ax.get_xlim()[1] - ax.get_xlim()[0]) * 0.02
    icon_center = icon_size / 2.0

    # Add the respective image to each node
    for n in G.nodes:
        xf, yf = tr_figure(pos[n])
        xa, ya = tr_axes((xf, yf))
        # get overlapped axes and plot icon
        a = plt.axes([xa - icon_center, ya - icon_center, icon_size, icon_size])
        a.imshow(icons[n])
        a.axis("off")

    if label == True:
        plt.savefig((save_path + '_weights.png').replace(' ', ''))
        plt.savefig((save_path + '_weights.eps').replace(' ', ''))
    else:
        plt.savefig((save_path + '.png').replace(' ', ''))
        plt.savefig((save_path + '.eps').replace(' ', ''))
    return

def generate_icons(dict_freq):
    """
        generate icons needed to plot as nodes for the graphs

        Parameters
        ----------
        dict_freq: dictionary containing the frequencies for the plots
    """
    frequencies = np.array(list(dict_freq.values()))
    colors = getcolors()
    # Create a figure and axis for each sine wave
    for freq in frequencies:
        for n, c in colors.items():
            # Create a time array from 0 to 2 * pi with a sufficient number of points
            t = np.linspace(0, 2, 1000)

            # Compute the sine wave for the given frequency
            y = np.sin(np.pi * freq * t)

            # Create a new figure and axis with no axis labels or ticks
            fig, ax = plt.subplots(figsize=(4, 4))
            ax.plot(t, y, c, linewidth=8)
            ax.axis('off')  # Turn off the axis

            # Save the figure as a PDF file
            filename = f"icons/sine_{freq}_{n}.png"
            plt.savefig(filename, bbox_inches='tight', transparent=False)
            filename = f"icons/sine_{freq}_{n}.eps"
            plt.savefig(filename, bbox_inches='tight', transparent=False)
            plt.close()

    print("Sine waves saved as png files.")
    return

def getcolors():
    colors = {
        'blue': '#4C72B0',
        'orange': '#DD8452',
        'green': '#55A868',
        'red': '#C44E52',
        'purple': '#8172B3'
    }
    return colors
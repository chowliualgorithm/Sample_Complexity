# ------------------------------ import packages ----------------------------- #
import numpy as np

import pandas as pd

import networkx as nx

from scipy.stats import entropy
from scipy.optimize import minimize, LinearConstraint, NonlinearConstraint, Bounds

from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianNetwork
from pgmpy.factors.discrete.CPD import TabularCPD

from itertools import product
import multiprocessing

# -------------------------------- Exact Rate -------------------------------- #

def get_exact_rate(model, num_init_values_eR):
    """
        returns the exact error rate for an arbitrary given model

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        num_init_values_eR: integer, defines the number of different starting points for the nonlinear optimization problem
    """
    # set edges
    exact_rate = 10
    e_inP = None
    e_ninP = None

    edge_prime = nx.complement(model.to_undirected()).edges
    for e_prime in edge_prime:
        path = nx.shortest_path(model.to_undirected(), source=e_prime[0], target=e_prime[1])
        for i in range(len(path) - 1):
            e = tuple((path[i], path[i + 1]))
            inference = VariableElimination(model)
            joint_dist_Pee = inference.query(variables=list(sorted(set(e + e_prime), key=list(e + e_prime).index)))
            for j in range(num_init_values_eR):
                initial_guess_temp = np.random.random(joint_dist_Pee.values.shape)
                initial_guess_temp = initial_guess_temp / np.sum(initial_guess_temp)

                exact_rate_temp, joint_dist_Q_temp = exact_Jee(model, e, e_prime, initial_guess_temp)

                if exact_rate_temp < exact_rate:
                    exact_rate = exact_rate_temp
                    e_inP = e
                    e_ninP = e_prime
    return exact_rate, e_inP, e_ninP

def mutual_info(model, edge):
    """
    returns the mutual information of the two variables of the given edge

    Parameters
    ----------
    model: BayesianNetwork from pgmpy.models
        cpd have to be given

    edge: tuple with the two nodes for which the mutual information has to be computed
    """
    # Compute Joint Distribution
    inference = VariableElimination(model)
    joint_dist_Pe = inference.query(variables=list(edge))
    marg_dist_e0 = inference.query(variables=[edge[0]])
    marg_dist_e1 = inference.query(variables=[edge[1]])

    mutual_info = 0
    for x in range(model.get_cpds(edge[0]).variable_card):
        for y in range(model.get_cpds(edge[0]).variable_card):
            mutual_info += joint_dist_Pe.values[x, y] * np.log(joint_dist_Pe.values[x, y] / (marg_dist_e0.values[x] * marg_dist_e1.values[y]))
    return mutual_info

def exact_Jee(model, edge1, edge2, initial_guess):
    """
        returns the error exponent of a crossover event for two edges computed as a solution of the
        nonlinear-optimization problem: min_Q KL(Q||P_{e,e'}) with condition MI(Q_e) = MI(Q_e')

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        edge1/edge2: two possible edges in the model, one has to be in the edge set of the true distribution, the other one not

        initial_guess: initial guess for the nonlinear_optimization problem
    """
    # switch edges in the case that edge2 is contained in path of edge1 (therefore edge2 is not contained in E_P
    pathset = []
    for path in nx.all_simple_paths(model, source=edge1[0], target=edge1[1]):
        pathset = set(pathset + path)
    if edge2[0] in pathset and not set(edge1).intersection(set(edge2)):
        dummy = edge1
        edge1 = edge2
        edge2 = dummy
    # switch order of edges such that the shared node is in the middle
    if len(set(edge1 + edge2)) == 3 and edge1[0] in edge2:
        edge1 = edge1[::-1]
    if len(set(edge1 + edge2)) == 3 and edge2[1] in edge1:
        edge2 = edge2[::-1]

    inference = VariableElimination(model)
    joint_dist = inference.query(variables=list(sorted(set(edge1 + edge2), key=list(edge1 + edge2).index)))
    Pee = joint_dist.values.flatten()
    def objective(Q, P):
        return entropy(Q, P)

    valid_distribution_constraint_1 = LinearConstraint(np.ones((len(Pee),)), 1, 1)#, keep_feasible=True)
    valid_distribution_constraint_2 = LinearConstraint(np.eye((len(Pee))), 0, 1)#, keep_feasible=True)

    if len(joint_dist.values.shape) == 3:
        def constraint_MI_3(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=2)
            Qeprime = np.sum(Qee, axis=0)

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]), 1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_3, 0, 0)
    else:
        def constraint_MI_4(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=(2, 3))
            Qeprime = np.sum(Qee, axis=(0, 1))

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]),1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_4, 0, 0)

    options = {'maxiter': 1e6,  # Maximum number of iterations
               'disp': False}   # Display convergence messages

    method='SLSQP'

    result = minimize(lambda Q: objective(Q, Pee), initial_guess.flatten(), constraints=[valid_distribution_constraint_1, valid_distribution_constraint_2, MI_constraint], method=method,options=options)

    return result.fun, np.reshape(result.x, joint_dist.values.shape)

# --------------------------- Compute Probability ---------------------------- #

def get_P1(num):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states
    """
    denom = sum(2**i for i in range(num))
    prob = np.reshape(np.array(2**np.arange(num)/denom), (num,1))
    return prob.tolist()

def get_Pi(num, gamma):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states

    gamma: parameter influencing the distance to the uniform distribution
    """
    g = gamma
    mat = np.ones((num,num))/num - np.ones((num,num))*g/(num-1) + np.eye(num)*(1 + 1/(num-1))*g
    return mat.tolist()

# ----------------------------- Generate Model ------------------------------- #

def get_complete_model(model_type, gamma, num_nodes, num_states):
    """
    returns a Bayesian Network with a given structure and the distribution is defined by get_P1() and get_Pi()

    Parameters
    ----------
    model_type: type of the model, either 'tree' for a binary tree 'star' or 'chain' possible

    gamma: parameter influencing the distance to the uniform distribution

    num_nodes: nodes the model should have

    num_states: number of possible states per node
    """
    tree = nx.DiGraph()
    edges = []

    if model_type == 'tree':
        for num in np.arange(1, num_nodes):
            if num % 2 == 1:
                edges.append(((num-1)/2, num))
            else:
                edges.append(((num - 2) / 2, num))
    elif model_type == 'star':
        for num in np.arange(1, num_nodes):
            edges.append((0, num))
    elif model_type == 'chain':
        for num in np.arange(1, num_nodes):
            edges.append((num - 1, num))

    tree.add_edges_from(edges)
    model_tree = BayesianNetwork(tree)
    cpd = TabularCPD(0, variable_card=num_states, values=get_P1(num_states))
    model_tree.add_cpds(cpd)
    mat = get_Pi(num_states, gamma)
    for e in model_tree.to_undirected().edges:
        if e[0] < e[1]:
            cpd = TabularCPD(e[1], variable_card=num_states, values=mat, evidence=[e[0]],
                             evidence_card=[num_states])
        else:
            cpd = TabularCPD(e[0], variable_card=num_states, values=mat, evidence=[e[1]],
                             evidence_card=[num_states])
        model_tree.add_cpds(cpd)
    return model_tree

# ----------------------------- Compute Gamma -------------------------------- #

def comp_gamma(*args):  # num_samples, model, num_runs):
    """
    function for parallel computation

    Parameters
    ----------
    args: (tuple, gamma, num_init, exact_rate_2states)

    tuple: tuple containing the number of nodes and the number of states

    gamma: parameter influencing the distance to the uniform distribution

    num_init: number of initial values for the nonlinear optimization problem returning the error exponent

    exact_rate_2states: objective value for the nonlinear optimization returning gamma for a higher number of nodes/states
    """
    tuple, gamma, num_init, exact_rate_2states, model_type = args[0]
    num_nodes = tuple[0]
    num_states = tuple[1]
    print('Start loop with ' + str(num_nodes) + ' nodes and ' + str(num_states) + ' states', flush=True)

    def objective(model_type, num_rv, gam, num_s, exact_rate_goal, num_in):
        model_tree = get_complete_model(model_type, gam, num_rv, num_s)
        exact_rate, _, _ = get_exact_rate(model_tree, num_in)
        return abs(exact_rate_goal-exact_rate)
    gamma_def_space = Bounds(lb=1e-10, ub=0.5-1e-10, keep_feasible=True)
    options = {'maxiter': 500,                                                  # Maximum number of iterations
               'disp': False,
               'tol': 1e-8}
    method='Nelder-Mead'

    result = minimize(lambda g: objective(model_type, num_nodes, g, num_states, exact_rate_2states, num_init), x0=gamma, bounds=gamma_def_space, method=method, options=options)

    model_comp = get_complete_model(model_type, result.x, num_nodes, num_states)
    exact_Kp, _, _ = get_exact_rate(model_comp, num_init)
    print('End loop with ' + str(num_nodes) + ' nodes and ' + str(num_states) + ' states', flush=True)
    print(str(num_nodes) + ' nodes and ' + str(num_states) + ' states:   ' + 'Gamma: ' + str(result.x[0]) + ', Kp: ' + str(exact_Kp), flush=True)
    return num_states, num_nodes, result.x[0], exact_Kp
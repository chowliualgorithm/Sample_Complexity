# ------------------------------ import packages ----------------------------- #
import numpy as np

import pandas as pd

import re

import networkx as nx

from scipy.stats import entropy, norm
from scipy.optimize import minimize, LinearConstraint, NonlinearConstraint, Bounds

from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianNetwork
from pgmpy.sampling import BayesianModelSampling
from pgmpy.factors.discrete.CPD import TabularCPD
from pgmpy.estimators import TreeSearch

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import font_manager
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import MultipleLocator, FixedLocator, MaxNLocator

from itertools import product
import multiprocessing

# -------------------------------- Exact Rate -------------------------------- #

def get_exact_rate(model, num_init_values_eR):
    """
        returns the exact error rate for an arbitrary given model

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        num_init_values_eR: integer, defines the number of different starting points for the nonlinear optimization problem
    """
    # set edges
    exact_rate = 10
    e_inP = None
    e_ninP = None

    edge_prime = nx.complement(model.to_undirected()).edges
    for e_prime in edge_prime:
        path = nx.shortest_path(model.to_undirected(), source=e_prime[0], target=e_prime[1])
        for i in range(len(path) - 1):
            e = tuple((path[i], path[i + 1]))
            inference = VariableElimination(model)
            joint_dist_Pee = inference.query(variables=list(sorted(set(e + e_prime), key=list(e + e_prime).index)))
            for j in range(num_init_values_eR):
                initial_guess_temp = np.random.random(joint_dist_Pee.values.shape)
                initial_guess_temp = initial_guess_temp / np.sum(initial_guess_temp)

                exact_rate_temp, joint_dist_Q_temp = exact_Jee(model, e, e_prime, initial_guess_temp)

                if exact_rate_temp < exact_rate:
                    exact_rate = exact_rate_temp
                    e_inP = e
                    e_ninP = e_prime
    return exact_rate, e_inP, e_ninP

def mutual_info(model, edge):
    """
    returns the mutual information of the two variables of the given edge

    Parameters
    ----------
    model: BayesianNetwork from pgmpy.models
        cpd have to be given

    edge: tuple with the two nodes for which the mutual information has to be computed
    """
    # Compute Joint Distribution
    inference = VariableElimination(model)
    joint_dist_Pe = inference.query(variables=list(edge))
    marg_dist_e0 = inference.query(variables=[edge[0]])
    marg_dist_e1 = inference.query(variables=[edge[1]])

    mutual_info = 0
    for x in range(model.get_cpds(edge[0]).variable_card):
        for y in range(model.get_cpds(edge[0]).variable_card):
            mutual_info += joint_dist_Pe.values[x, y] * np.log(joint_dist_Pe.values[x, y] / (marg_dist_e0.values[x] * marg_dist_e1.values[y]))
    return mutual_info

def exact_Jee(model, edge1, edge2, initial_guess):
    """
        returns the error exponent of a crossover event for two edges computed as a solution of the
        nonlinear-optimization problem: min_Q KL(Q||P_{e,e'}) with condition MI(Q_e) = MI(Q_e')

        Parameters
        ----------
        model: BayesianNetwork from pgmpy.models
            cpd have to be given

        edge1/edge2: two possible edges in the model, one has to be in the edge set of the true distribution, the other one not

        initial_guess: initial guess for the nonlinear_optimization problem
    """
    # switch edges in the case that edge2 is contained in path of edge1 (therefore edge2 is not contained in E_P
    pathset = []
    for path in nx.all_simple_paths(model, source=edge1[0], target=edge1[1]):
        pathset = set(pathset + path)
    if edge2[0] in pathset and not set(edge1).intersection(set(edge2)):
        dummy = edge1
        edge1 = edge2
        edge2 = dummy
    # switch order of edges such that the shared node is in the middle
    if len(set(edge1 + edge2)) == 3 and edge1[0] in edge2:
        edge1 = edge1[::-1]
    if len(set(edge1 + edge2)) == 3 and edge2[1] in edge1:
        edge2 = edge2[::-1]

    inference = VariableElimination(model)
    joint_dist = inference.query(variables=list(sorted(set(edge1 + edge2), key=list(edge1 + edge2).index)))
    Pee = joint_dist.values.flatten()
    def objective(Q, P):
        return entropy(Q, P)

    valid_distribution_constraint_1 = LinearConstraint(np.ones((len(Pee),)), 1, 1)
    valid_distribution_constraint_2 = LinearConstraint(np.eye((len(Pee))), 0, 1)

    if len(joint_dist.values.shape) == 3:
        def constraint_MI_3(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=2)
            Qeprime = np.sum(Qee, axis=0)

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]), 1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_3, 0, 0)
    else:
        def constraint_MI_4(Q):  # Mutual information are the same
            Qee = Q.reshape(joint_dist.values.shape)  # first two axes are from edge2, the others from edge1

            Qe = np.sum(Qee, axis=(2, 3))
            Qeprime = np.sum(Qee, axis=(0, 1))

            MI_e = 0
            MI_eprime = 0
            for i in range(len(Qe[0, :])):
                MI_e += np.sum(Qe[:, i] * np.log(np.maximum(Qe[:, i] / (np.sum(Qe, axis=1) * np.sum(Qe, axis=0)[i]),1e-8)))
            for i in range(len(Qeprime[0, :])):
                MI_eprime += np.sum(Qeprime[:, i] * np.log(np.maximum(Qeprime[:, i] / (np.sum(Qeprime, axis=1) * np.sum(Qeprime, axis=0)[i]),1e-8)))
            return MI_e - MI_eprime
        MI_constraint = NonlinearConstraint(constraint_MI_4, 0, 0)

    options = {'maxiter': 1e6,  # Maximum number of iterations
               'disp': False}   # Display convergence messages

    method='SLSQP'

    result = minimize(lambda Q: objective(Q, Pee), initial_guess.flatten(), constraints=[valid_distribution_constraint_1, valid_distribution_constraint_2, MI_constraint], method=method,options=options)

    return result.fun, np.reshape(result.x, joint_dist.values.shape)

# --------------------------- Compute Probability ---------------------------- #

def get_P1(num):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states
    """
    denom = sum(2**i for i in range(num))
    prob = np.reshape(np.array(2**np.arange(num)/denom), (num,1))
    if np.sum(prob) != 1:
        prob[0] -= np.sum(prob) - 1
    return prob.tolist()

def get_Pi(num, gamma):
    """
    returns a distribution for the root node, elements have the probability 2**i/sum(P1)

    Parameters
    ----------
    num: number of states

    gamma: parameter influencing the distance to the uniform distribution
    """
    g = gamma
    mat = np.ones((num,num))/num - np.ones((num,num))*g/(num-1) + np.eye(num)*(1 + 1/(num-1))*g
    if any(np.sum(mat, axis=0) - 1) != 0:
        n = mat.shape[0]
        mat[range(n), range(n)] -= list(np.sum(mat, axis=0) - 1)
    return mat.tolist()

# ----------------------------- Generate Model ------------------------------- #

def get_complete_model(model_type, gamma, num_nodes, num_states):
    """
    returns a Bayesian Network with a given structure and the distribution is defined by get_P1() and get_Pi()

    Parameters
    ----------
    model_type: type of the model, either 'tree' for a binary tree 'star' or 'chain' possible

    gamma: parameter influencing the distance to the uniform distribution

    num_nodes: nodes the model should have

    num_states: number of possible states per node
    """
    tree = nx.DiGraph()
    edges = []

    if model_type == 'tree':
        for num in np.arange(1, num_nodes):
            if num % 2 == 1:
                edges.append(((num-1)/2, num))
            else:
                edges.append(((num - 2) / 2, num))
    elif model_type == 'star':
        for num in np.arange(1, num_nodes):
            edges.append((0, num))
    elif model_type == 'chain':
        for num in np.arange(1, num_nodes):
            edges.append((num - 1, num))

    tree.add_edges_from(edges)
    model_tree = BayesianNetwork(tree)
    cpd = TabularCPD(0, variable_card=num_states, values=get_P1(num_states))
    model_tree.add_cpds(cpd)
    mat = get_Pi(num_states, gamma)
    for e in model_tree.to_undirected().edges:
        if e[0] < e[1]:
            cpd = TabularCPD(e[1], variable_card=num_states, values=mat, evidence=[e[0]],
                             evidence_card=[num_states])
        else:
            cpd = TabularCPD(e[0], variable_card=num_states, values=mat, evidence=[e[1]],
                             evidence_card=[num_states])
        model_tree.add_cpds(cpd)
    return model_tree

# -------------------------- Compute Sample Number --------------------------- #

def get_error(num_samples, model):
    """
        returns 1 if an error event occured and 0 if the algorithm returned the model correctly from a given number of samples

        Parameters
        ----------
        num_samples: integer defining the number of samples used for reconstruction

        model: BayesianNetwork from pgmpy.models
            cpd have to be given
    """
    # ----------------------------- generate samples ----------------------------- #
    # sample data from BN tree structured
    inference_tree = BayesianModelSampling(model)
    df_data_tree = inference_tree.forward_sample(size=num_samples, show_progress=False)

    # ----------------------------- Chow Liu Algorithm --------------------------- #
    est = TreeSearch(df_data_tree)
    dag = est.estimate(estimator_type="chow-liu", show_progress=False)

    edgeset_true = set(model.to_undirected().edges)
    edgeset_CL = set(dag.to_undirected().edges)
    for el in edgeset_CL:
        if el not in edgeset_true and (el[1],el[0]) not in edgeset_true:
            return 1
    return 0

def get_errorprobability(model, m, n):
    """
        runs the experiment for a certain number of times and counts the errors
        to approximate the error probability by the mean number errors/m

        Parameters
        ----------
        n:              number of samples

        model:          networkx bayesian model

        m:              number of times the algorithm reconstructs a tree with n random samples
    """
    iterator = range(m)
    result = [get_error(n, model) for _ in iterator]
    num_error = np.sum(result)/m
    return num_error

def find_exact_n(model, m, n, th):
    """
        algorithm to get the smallest n such that the approximate error probability is below some threshold
        starts with small n because of computational effort and ceils the number of n when the minimum stepsize
        is reached.

        Parameters
        ----------
        n:              number of samples from upper bound

        model:          networkx bayesian model

        m:              number of times the algorithm reconstructs a tree with n random samples

        th:             threshold
    """
    temp_n = int(max(5, np.ceil(n / 10000000)))
    P_init = get_errorprobability(model, m, temp_n)
    while P_init > th:
        temp_n = int(temp_n*10)
        P_init = get_errorprobability(model, m, temp_n)
    ubi = int(temp_n)
    lbi = int(temp_n/10)
    for i in range(3): # alternatively use while (ubi - lbi) < threshold for desired accuraccy (Caution, because of using sample proportion never completely exact!)
        n_mid = int(np.ceil((ubi + lbi) / 2))
        P = get_errorprobability(model, m, n_mid)
        if P < th:
            ubi = n_mid
        else:
            lbi = n_mid
    return ubi

def get_n_upperbound(num_alp, num_node, Kp, threshold, init_step=int(1e7)):
    """
        returns the exact number of samples such that the upper bound for the error probability
        provided in the paper of Vincent Y. F. Tan, A Large-Deviation Analysis of the Maximum-Likelihood
        Learning of Markov Tree Structures, 2011, DOI: 10.1109/TIT.2011.2104513
        is smaller than the given threshold

        Parameters
        ----------
        num_alp:    number of states

        num_node:   number of nodes

        Kp:         error exponent of the model

        threshold:  threshold for the error probability
    """
    n = 1
    while True:
        sum_log_n = np.sum(np.log(np.arange(n) + 1))
        sum_log_n_chi = np.sum(np.log(np.arange(num_alp ** 4, n + num_alp ** 4)))
        while - sum_log_n + sum_log_n_chi - n*Kp > np.log(2*threshold/(num_node - 1)**2/(num_node - 2)):
            n += init_step
            sum_log_n = np.sum(np.log(np.arange(n) + 1))
            sum_log_n_chi = np.sum(np.log(np.arange(num_alp ** 4, n + num_alp ** 4)))

        if init_step == 1:
            break

        n -= init_step
        init_step = int(init_step / 10)
    return n

def get_n(*args):
    """
        function for parallel computation

        Parameters
        ----------
        args: tuple(num_nodes, num_states, m, Kp, th, model_type, gamma, n_it)
            num_nodes:    number of nodes of the model (can be different for different processes)
            num_states:   number of states of the model (can be different for different processes)
            m:            number of runs of the experiment to estimate the error probability (can be different for different processes)
            Kp:           minimal error exponent
            th:           threshold for the error probability
            model_type:   'star', 'chain' or binary 'tree' possible
            gamma:        gamma wrt to the model distribution
            CI:           confidence interval
            n_it:         True or False, if True then the approximate number of samples is computed wrt the confidence interval and thresholds
    """
    num_nodes, num_states, m, Kp, th, marg, model_type, gamma, CI, n_it = args[0]
    print('Start loop with ' + str(num_nodes) + ' nodes and ' + str(num_states) + ' states and ' + str(gamma) + ' gamma and ' + str(CI) + ' confidence interval ', flush=True)
    model = get_complete_model(model_type, gamma, num_nodes, num_states)
    if m == None:
        m = int(max(np.ceil(np.power(norm.ppf(CI) / marg * np.sqrt((th + marg) * (1 - (th + marg))), 2)), np.ceil(6 / (th * (1 - th)))))
    if Kp == None:
        Kp = get_exact_rate(model, 25)
    bound_num_samples = get_n_upperbound(num_states, num_nodes, Kp, th+marg) # compute upper bound for n according to Tan (2011)
    print(bound_num_samples, CI, gamma)

    if n_it == True:
        th_n = find_exact_n(model, int(m), bound_num_samples, th)
        print('End loop with ' + str(num_nodes) + ' nodes and ' + str(num_states) + ' states and ' + str(gamma) + ' gamma and ' + str(CI) + ' confidence interval ', flush=True)
        print((num_nodes, num_states, bound_num_samples, th_n))
        return (num_nodes, num_states, bound_num_samples, th_n)
    else:
        print('End loop with ' + str(num_nodes) + ' nodes and ' + str(num_states) + ' states and ' + str(gamma) + ' gamma and ' + str(CI) + ' confidence interval ', flush=True)
        return (num_nodes, num_states, bound_num_samples)

def log_tick_formatter(val, pos=None):
    return f"10$^{{{int(val)}}}$"

def plot_3D_n(df, name, model, th, eps):
    font_path = 'Add_Path/.../Fonts/LatinmodernmathRegular-z8EBa.otf'  # Your font path goes here
    font_manager.fontManager.addfont(font_path)
    prop = font_manager.FontProperties(fname=font_path)

    plt.rcParams['font.family'] = 'sans-serif'
    plt.rcParams['font.sans-serif'] = prop.get_name()
    plt.rcParams["font.size"] = "18"
    print(prop.get_name())

    col = getcolors()

    ub_n = df[['threshold (Tan)']].unstack().to_numpy()
    real_n = None
    if df.shape[1] == 2:
        real_n = df[['threshold (real)']].unstack().to_numpy()

    xlab = df.index.names[0]
    ylab = df.index.names[1]

    index = np.array(list(df.index))

    x = np.array(list(set(index[:,0])))
    y = np.array(sorted(list(set(index[:, 1]))))

    X, Y = np.meshgrid(x, y)

    fig = plt.figure(figsize=(10, 8))
    ax = Axes3D(fig, auto_add_to_figure=False)
    fig.add_axes(ax)

    ax.set_title('Number of Samples for ' + model)
    ax.set_xlim3d(np.min(x), np.max(x))
    ax.set_ylim3d(np.min(y), np.max(y))
    ax.zaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    if df.shape[1] != 2:
        ax.set_zlim3d(max(0, np.floor(np.log10(np.min(ub_n)))), np.ceil(np.log10(np.max(ub_n))))
    else:
        ax.set_zlim3d(max(0, np.floor(np.log10(np.min(real_n)))), np.ceil(np.log10(np.max(ub_n))))

    ax.set(xlabel=xlab, ylabel=ylab, zlabel='Number of Samples')
    ax.set_title('Required Sample Size for ' + model)

    ax.xaxis.labelpad = 10
    ax.yaxis.labelpad = 10
    ax.zaxis.labelpad = 10

    ax.view_init(elev=17, azim=-60)

    ax.grid(True)
    ax.xaxis.pane.set_edgecolor('black')
    ax.yaxis.pane.set_edgecolor('black')
    ax.zaxis.pane.set_edgecolor('black')
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    if xlab == 'Confidence Interval':
        ax.xaxis.set_major_locator(FixedLocator(x[:-1]))
    else:
        ax.xaxis.set_major_locator(FixedLocator(x))
    if ylab == 'Confidence Interval':
        ax.yaxis.set_major_locator(FixedLocator(y[:-1]))
    else:
        ax.yaxis.set_major_locator(FixedLocator(y))
    ax.zaxis.set_major_locator(MultipleLocator(1))

    # Z minor ticks
    zminorticks = []
    zaxmin, zaxmax = ax.get_zlim()
    for zorder in np.arange(np.floor(zaxmin), np.ceil(zaxmax)):
        zminorticks.extend(np.log10(np.linspace(2, 9, 8)) + zorder)
    ax.zaxis.set_minor_locator(FixedLocator(zminorticks))

    surf = ax.plot_surface(X, Y, np.log10(ub_n.T), color=col['green'], lw=0.1, alpha=0.8)

    ax.plot_wireframe(X, Y, np.log10(ub_n.T), linewidth=1, color=col['green'])
    ax.contour(X, Y, np.log10(ub_n.T), 4, colors=col['green'], linestyles="solid")

    if real_n is not None:
        surf2 = ax.plot_surface(X, Y, np.log10(real_n.T), color=col['orange'], lw=0.1, alpha=0.8)
        ax.plot_wireframe(X, Y, np.log10(real_n.T), linewidth=1, color=col['orange'])
        ax.contour(X, Y, np.log10(real_n.T), 4, colors=col['orange'], linestyles="solid")

    if ylab == 'Number of Nodes' or ylab == 'Number of States':
        ax.view_init(8, -140)
    else:
        ax.view_init(4, 30)

    plt.tight_layout()

    plt.savefig(name + '.png')
    plt.savefig(name + '.eps')

    plt.show(block=False)

    plt.rcParams["font.size"] = "32"
    # fig2, (ax1, ax2, ax3, ax4) = plt.subplots(2, 2)
    fig2, ax1 = plt.subplots(1, 2, figsize=(15, 6.5))
    ax1[0].set_yscale('log')
    ax1[1].set_yscale('log')
    if ylab == 'Gamma':
        ax1[0].xaxis.set_major_locator(FixedLocator(y))
        ax1[1].xaxis.set_major_locator(FixedLocator(y))
        ax1[0].plot(y, (ub_n.T)[:, 0], '-', color=col['green'])
        ax1[0].set_xlabel(ylab)
        ax1[1].plot(y, (ub_n.T)[:, -1], '-', color=col['green'])
        ax1[1].set_xlabel(ylab)
        if real_n is not None:
            ax1[0].plot(y, (real_n.T)[:, 0], '-', color=col['orange'])
            ax1[1].plot(y, (real_n.T)[:, -1], '-', color=col['orange'])
    else:
        ax1[0].xaxis.set_major_locator(MaxNLocator(integer=True))
        ax1[1].xaxis.set_major_locator(MaxNLocator(integer=True))
        ax1[0].plot(y.astype(int), (ub_n.T)[:, 0], '-', color=col['green'])
        ax1[0].set_xlabel(ylab)
        ax1[1].plot(y.astype(int), (ub_n.T)[:, -1], '-', color=col['green'])
        ax1[1].set_xlabel(ylab)
        if real_n is not None:
            ax1[0].plot(y.astype(int), (real_n.T)[:, 0], '-', color=col['orange'])
            ax1[1].plot(y.astype(int), (real_n.T)[:, -1], '-', color=col['orange'])

    plt.tight_layout()

    plt.savefig(name + '_contour.png')
    plt.savefig(name + '_contour.eps')

    plt.show()
def getcolors():
    colors = {
        'blue': '#4C72B0',
        'orange': '#DD8452',
        'green': '#55A868',
        'red': '#C44E52',
        'purple': '#8172B3'
    }
    return colors